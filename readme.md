My solutions are in the subdirectories. Here below I link to note worthy solutions from other people.

Check out [sakisan.be/advent-of-code.html](http://sakisan.be/advent-of-code.html) for a live instance of the Elm solutions.

**Day 01**

- [Haskell: list comprehension can do it all](https://www.reddit.com/r/adventofcode/comments/k4e4lm/2020_day_1_solutions/ge8dv07) by selfishgene9
- [Elm: filter on the tail to avoid unnecessary combinations](https://ellie-app.com/bH4bLkcb3x8a1) by mbaumann


**Day 02**

- [Haskell: custom xor operator](https://topaz.github.io/paste/#XQAAAQDxBAAAAAAAAAAW4HygYTdUxLMR6uCYSrdk1W+rlYGZKP8Stt86mdrdS5cVkJkqan+OrG426mGijR/BQB4GHFTTfkIQIFlXTNz2D/cZsogfIDqXdho1SHd89xZvIfcvXf95+Tl4f+xZkKJgrnf+iEdoiWHkp1BK4lpd+FWPbgfQd7p/4W4u+mpa7Q2t1A+ctF7u8rwJQXUjdWGmlA9EdbfpGq9eRjN33/4aP+wfefrRZbIEiFzMSz1WBe/r1PUhu9l3hogsznncUGXljwaDlL7vZsrSGNbhmKel5PaYWrtbX3LHyxV6R76NU5jb2/mVnDyiV6o/h2TwcCp35eh/YOpzwBRM7oOT37EqR3CR60wIWFnuQ4RrGH/+wxtEfELyYMfUd2gLQJ2+zq2RzWjC9/ONFQDkBvjfoJYPyjLoMAZU0JYyQT2YJPDcGfsFFeBlsPtAaxWsko9DdorpPnJdkCMXSuufYWgz/jN8hQ==) by baktix

**Day 03**

- [Haskell: map cycle . lines and keep on incrementing x](https://git.sr.ht/~ehamberg/aoc/tree/main/2020/03/Main.hs) by ehamberg
- [C# (but FP): treating the grid a single line](https://github.com/tkshill/advent-of-code-2020/blob/main/AOC2020/DayThree/DayThree.cs) by tkshill

**Day 04**

- [Haskell: maybe, inRange, Map.!?, readMaybe, mapM_, ... ](https://www.reddit.com/r/adventofcode/comments/k6e8sw/2020_day_04_solutions/gekqyh2?utm_source=share&utm_medium=web2x&context=3) by ephemient
- [Elm: nice Parsers](https://github.com/manuscrypt/aoc2020/blob/main/day4/src/Parsers.elm) by mbaumann

**Day 05**

- [Haskell: readInt 2](https://git.sr.ht/~ehamberg/aoc/tree/main/2020/05/Main.hs) by ehamberg

**Day 06**

suggestions?

**Day 07**

- [Haskell: consize, unfoldr, do-notation for lists](https://www.reddit.com/r/adventofcode/comments/k8a31f/2020_day_07_solutions/gex9ebh?utm_source=share&utm_medium=web2x&context=3) by ephemient

**Day 08**

- [Haskell: example of a Read instance implemented with Text.ParserCombinators.ReadP](https://gist.github.com/incertia/31a3334df08593ad14f653e6dba56458) by incertia

**Day 09**

- [Haskell: guided sublists, expanding and shrinking from left to right as needed](https://www.reddit.com/r/adventofcode/comments/k9lfwj/2020_day_09_solutions/gf54kqt?utm_source=share&utm_medium=web2x&context=3) by Rick-T
- [Haskell: Most elegant way to check 2 distinct numbers for their sum](https://github.com/SimonBaars/AdventOfCode-Haskell/blob/master/2020/Day9.hs) by SimonBaars

**Day 10**

- [Haskell: only diffs of 1 contribute to part2 and their impact is predictable ](https://topaz.github.io/paste/#XQAAAQBjBAAAAAAAAAA2m8ixrhLu7YJFrd2FLde+PAG1Aui2yN36LC93WIQ2APMCiCS0L/ERCZO1Ub196UifbJLLVlbSvYxOwAjYDrQYBebXdg1tlZnkpCddIzEyACJhqVaVyfEHliHhHTo43RuH6MUu0/w7UwQVDRxlqCPYNFIpYzS1qKJg/dS0LL2ErMa11sDhjnpt6/o51Ih9QtTRsUzO6GnqZLny1iBhMg0nSw2pDcXMryFYJwvLEhhnOA2VNJhWM1Gn+OCdev9TOOzuOQowvMP36UPzxmVWAUAZjDS15RcLbFFeojz/Q3BXsrrnJMBiNxhpkR4YrDjCAuCJQhksI2fE4chaAJFvJaPw5UlpCtQuiVbbqc3yXAqMc7ZCzCRvX+0srUpCaftCCjjva+MKRJRQzBKSunnKeBkSDwczfvMFeCOMUAgJNzevKG/9iDc4NV+n56zO45uDz83yZF37h0INKpfAtZ1zhvULILedD6BLrh+7SiN9L/heBGFZY/MeDx08C7N1dnUipMqmyHZYt3OLuS/tMCidJlWOzC47ZbSiX3wpWhz5pYzuaeAAOqt+/Rvbsm8RPpwnCrilLLtPJxBszSf/L+y/EDDgy6BfAaMq+4xMSOts+1hnxNL2wkja2D5aDr94d7rWSrcLEvswpKdPekGRmAJ9ph5eo4Eof0NKgZqb7BZ8WjzkDxcRvsq0aPFYPzt7XrE0PcKuY2DIHRC6dJvlmMk3QK3n9jjwd6jRN1F9n0ggIjvf5dh9uRrJ3uPhP6jwta+yGXyYZCnlkS59jZZLNJ9M8WPtctV6p2PKoZWKVLpONniR9BG3kjjVnL+SSlDSKZT1fkyu0MUzSNUiiu6H/vtZjJ4=) by Zealousideal-Track82
- [Python: just sum the previous 3 (if diff <= 3))](https://topaz.github.io/paste/#XQAAAQDYAQAAAAAAAAA7mkrvHeIvDZUizuO2LI0KXEPUFLLgeIPRqbv/z+nmNU7MYNHuAOqKaD9UwePzSwp6PCMKtAtFNW9cxtmVIYz3IvNWUuMsDps9d+dYVFeHkbd0ySJ3+osueO4GFwDiqdAKHhCCJ14Praey++DxNk5DSjBdNI1NLzlmnDiusV8tczbpafrYWnR5aUWEaceBINTQ4Jz1Cn1XKBUngAxgHT4iJoqDEBeJvQQUhhgyoWLzARcuKNWIc7xHhmZZampTN0JLw8obO3PGA0WOqs6D5eO1i+42XoQanjy1Cicf+UvMddA1UIhME0at9K4WpZNPPBPqKll+k1Y9dPX/tibHYA==) by nthistle  
note: recursive solutions really do the same as dynamic programming solutions, but instead of adding the previous 3 we add the next 3. Both work cause the problem is symmetric. But the recursive is more complicated because we don't know the values of N+1,N+2,N+3 until we reach the end.

**Day 11**

- [Haskell: iterate, Set & Map, V2 points + some AoC specific helper functions](https://github.com/mstksg/advent-of-code-2020/blob/master/reflections.md#day-11) by mstksg (this repo is a gold mine btw)

**Day 12**

- [Haskell: Directions as a Group, Semigroup and Monoid](https://github.com/mstksg/advent-of-code-2020/blob/master/reflections.md#day-11) by mstksg

**Day 13**

- [Mathematica: let the software solve the equations](https://github.com/Janiczek/advent-of-code/blob/master/Year2020/Day13Part2.nb) by Janiczek

**Day 15**

- [Haskell: performance with a mutable unboxed vector, featuring StateT monad](https://github.com/mstksg/advent-of-code-2020/blob/master/reflections.md#day-15) by mstksg

**Day 17**

- [Haskell: restrictKeys and withoutKeys](https://github.com/MatthiasCoppens/AOC2020/blob/master/day17/solution.hs) by MatthiasCoppens
- [Haskell: coordinates as bytes for performance](https://github.com/ephemient/aoc2020/blob/main/hs/src/Day17.hs) by ephemient

**Day 18**

- [Haskell: Parsec](https://git.sr.ht/~ehamberg/aoc/tree/main/2020/18/Main.hs) by ehamberg
- [Elm: Pratt Parser](https://github.com/manuscrypt/aoc2020/blob/main/day18/src/Main.elm) by mbaumann
- [Python: eval and regex replacements](https://github.com/5joshi/AoC/blob/master/2020/day18.py) by 5joshi

**Day 19**

- [Haskell: parser combinators, huh](https://github.com/glguy/advent2020/blob/master/execs/Day19.hs) by glguy
