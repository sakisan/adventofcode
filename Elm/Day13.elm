module Day13 exposing (..)


solve : String -> Maybe ( Int, Int )
solve input =
    parse input
        |> Maybe.andThen
            (\( time, busses ) ->
                part1 time busses
                    |> Maybe.map (\p1 -> ( p1, part2 busses ))
            )


parse : String -> Maybe ( Int, List ( Int, Int ) )
parse input =
    let
        lines =
            String.lines input

        earliest =
            Maybe.andThen String.toInt (List.head lines)

        list =
            lines
                |> List.drop 1
                |> List.head
                |> Maybe.map busTimes

        busTimes line =
            line
                |> String.split ","
                |> List.indexedMap
                    (\i x ->
                        String.toInt x
                            |> Maybe.map (\n -> ( i, n ))
                    )
                |> List.filterMap identity
    in
    Maybe.map2 Tuple.pair earliest list


part1 : Int -> List ( Int, Int ) -> Maybe Int
part1 earliest busses =
    busses
        |> List.map (\( _, bus ) -> ( bus, timeForBus earliest bus ))
        |> List.sortBy Tuple.second
        |> List.head
        |> Maybe.map (\( bus, time ) -> (time - earliest) * bus)


timeForBus : Int -> Int -> Int
timeForBus earliest bus =
    let
        q =
            (earliest // bus) * bus
    in
    if q == earliest then
        earliest

    else
        q + bus


part2 : List ( Int, Int ) -> Int
part2 busses =
    let
        sorted =
            List.sortBy Tuple.second busses

        nextFold ( t, bus ) ( start, increment ) =
            ( solveForBus ( t, bus ) ( start, increment )
            , lcm bus increment
            )

        ( time, _ ) =
            List.foldl nextFold ( 0, 1 ) sorted
    in
    time


solveForBus : ( Int, Int ) -> ( Int, Int ) -> Int
solveForBus ( t, bus ) ( start, increment ) =
    let
        solver x =
            if modBy bus (x + t) == 0 then
                x

            else
                solver (x + increment)
    in
    solver start


lcm : Int -> Int -> Int
lcm x y =
    case ( x, y ) of
        ( _, 0 ) ->
            0

        ( 0, _ ) ->
            0

        _ ->
            y * (x // gcd x y)


gcd : Int -> Int -> Int
gcd x y =
    if y == 0 then
        x

    else
        gcd y (modBy y x)
