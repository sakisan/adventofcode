module Day14 exposing (..)

import IntDict exposing (IntDict)


solve : String -> Maybe ( Int, Int )
solve input =
    Just ( part1 input, part2 input )


part1 : String -> Int
part1 input =
    List.foldl processLine ( IntDict.empty, "" ) (String.lines input)
        |> Tuple.first
        |> IntDict.values
        |> List.sum


processLine : String -> ( IntDict Int, String ) -> ( IntDict Int, String )
processLine line ( memory, mask ) =
    if String.startsWith "mask = " line then
        ( memory, String.dropLeft 7 line )

    else
        let
            start =
                List.head (String.indexes "[" line)

            end =
                List.head (String.indices "]" line)

            lastSpace =
                String.indexes " " line
                    |> List.reverse
                    |> List.head
                    |> Maybe.map (\l -> String.length line - l - 1)
        in
        case
            ( Maybe.map2 (\s e -> String.slice (s + 1) e line) start end
                |> Maybe.andThen String.toInt
            , Maybe.andThen (\s -> String.toInt (String.right s line)) lastSpace
            )
        of
            ( Nothing, _ ) ->
                Debug.todo "no"

            ( _, Nothing ) ->
                Debug.todo (Debug.toString ( line, ( start, end ), lastSpace ))

            ( Just address, Just value ) ->
                ( apply address value mask memory, mask )


apply : Int -> Int -> String -> IntDict Int -> IntDict Int
apply address value mask memory =
    IntDict.insert address (applyMask mask value) memory


applyMask : String -> Int -> Int
applyMask mask value =
    let
        binary =
            toBinary value

        paddedBinary =
            String.concat (List.repeat (String.length mask - String.length binary) "0")
                ++ binary
    in
    List.map2 applyBit (String.toList mask) (String.toList paddedBinary)
        |> String.fromList
        |> fromBinary


applyBit : Char -> Char -> Char
applyBit x b =
    if x == 'X' then
        b

    else
        x


toBinary : Int -> String
toBinary n =
    let
        a =
            if n > 1 then
                toBinary (n // 2)

            else
                ""

        b =
            if modBy 2 n == 0 then
                "0"

            else
                "1"
    in
    a ++ b


fromBinary : String -> Int
fromBinary =
    String.foldl (\x a -> 2 * a + readInt x) 0


readInt : Char -> Int
readInt char =
    case char of
        '0' ->
            0

        '1' ->
            1

        _ ->
            Debug.todo ("not a binary int " ++ Debug.toString char)


part2 : String -> Int
part2 input =
    List.foldl processLine2 ( IntDict.empty, "" ) (String.lines input)
        |> Tuple.first
        |> IntDict.values
        |> List.sum


processLine2 : String -> ( IntDict Int, String ) -> ( IntDict Int, String )
processLine2 line ( memory, mask ) =
    if String.startsWith "mask = " line then
        ( memory, String.dropLeft 7 line )

    else
        let
            start =
                List.head (String.indexes "[" line)

            end =
                List.head (String.indices "]" line)

            lastSpace =
                String.indexes " " line
                    |> List.reverse
                    |> List.head
                    |> Maybe.map (\l -> String.length line - l - 1)
        in
        case
            ( Maybe.map2 (\s e -> String.slice (s + 1) e line) start end
                |> Maybe.andThen String.toInt
            , Maybe.andThen (\s -> String.toInt (String.right s line)) lastSpace
            )
        of
            ( Nothing, _ ) ->
                Debug.todo "no"

            ( _, Nothing ) ->
                Debug.todo (Debug.toString ( line, ( start, end ), lastSpace ))

            ( Just address, Just value ) ->
                ( apply2 address value mask memory, mask )


apply2 : Int -> Int -> String -> IntDict Int -> IntDict Int
apply2 address value mask memory =
    List.foldl (\a -> IntDict.insert a value) memory (addresses mask address)


addresses : String -> Int -> List Int
addresses mask address =
    let
        numberOfXs =
            List.length (String.indices "X" mask)

        ps =
            combinations (2 ^ numberOfXs - 1)
                |> List.map pad

        pad binary =
            (List.repeat (numberOfXs - List.length binary) '0') ++ binary
    in
    List.map (\p -> applyMask2 p mask address) ps


applyMask2 : List Char -> String -> Int -> Int
applyMask2 replacements mask value =
    let
        binary =
            toBinary value

        paddedBinary =
            String.concat (List.repeat (String.length mask - String.length binary) "0")
                ++ binary
    in
    List.map2 applyBit2 (String.toList mask) (String.toList paddedBinary)
        |> applyReplacements replacements
        |> String.fromList
        |> fromBinary


applyBit2 : Char -> Char -> Char
applyBit2 x b =
    if x == 'X' then
        'X'

    else if x == '1' then
        '1'

    else
        b


applyReplacements : List Char -> List Char -> List Char
applyReplacements replacements string =
    case ( replacements, string ) of
        ( [], _ ) ->
            string

        ( _, [] ) ->
            string

        ( r :: rs, 'X' :: xs ) ->
            r :: applyReplacements rs xs

        ( _, x :: xs ) ->
            x :: applyReplacements replacements xs


combinations : Int -> List (List Char)
combinations n =
    List.range 0 n
        |> List.map (toBinary >> String.toList)


input1 =
    [ "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
    , "mem[8] = 11"
    , "mem[7] = 101"
    , "mem[8] = 0"
    ]


input2 =
    [ "mask = 000000000000000000000000000000X1001X"
    , "mem[42] = 100"
    , "mask = 00000000000000000000000000000000X0XX"
    , "mem[26] = 1"
    ]
