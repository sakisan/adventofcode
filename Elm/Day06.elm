module Day06 exposing (..)

import Set

solve : String -> Maybe ( Int, Int )
solve input = Just ( part1 input, part2 input )

part1 = groups >> List.map count1 >> List.sum
count1 = characterSet >> Set.size
groups = String.split "\n\n"
characterSet = String.toList >> Set.fromList

part2 = groups >> List.map count2 >> List.sum
count2 = String.lines >> List.map characterSet >> bind (List.foldl Set.intersect) firstSet >> Set.size
bind f g x = f (g x) x
firstSet = List.head >> Maybe.withDefault Set.empty
