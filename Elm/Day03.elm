module Day03 exposing (..)

import Dict exposing (Dict)


solve : String -> Maybe ( Int, Int )
solve input =
    String.lines input
        |> toDict
        |> Maybe.map count


type alias Woods =
    { trees : Dict ( Int, Int ) Bool
    , bounds : { x : Int, y : Int }
    }


toDict : List String -> Maybe Woods
toDict lines =
    let
        characters : List (List Char)
        characters =
            List.map String.toList lines

        trees : Dict ( Int, Int ) Bool
        trees =
            List.indexedMap
                (\y line ->
                    List.indexedMap
                        (\x character -> ( ( x, y ), character == '#' ))
                        line
                )
                characters
                |> List.concat
                |> Dict.fromList

        maybeXBound : Maybe Int
        maybeXBound =
            Maybe.map List.length (List.head characters)

        yBound : Int
        yBound =
            List.length characters
    in
    Maybe.map
        (\xBound ->
            { trees = trees
            , bounds = { x = xBound, y = yBound }
            }
        )
        maybeXBound


count : Woods -> ( Int, Int )
count woods =
    ( countTrees woods ( 3, 1 ) ( 0, 0 ) 0
    , [ ( 1, 1 ), ( 1, 3 ), ( 1, 5 ), ( 1, 7 ), ( 2, 1 ) ]
        |> List.map (\d -> countTrees woods d ( 0, 0 ) 0)
        |> List.product
    )


countTrees : Woods -> ( Int, Int ) -> ( Int, Int ) -> Int -> Int
countTrees ({ trees, bounds } as woods) ( dx, dy ) ( x, y ) n =
    if y >= bounds.y then
        n

    else
        countTrees woods ( dx, dy ) ( modBy bounds.x (x + dx), y + dy ) <|
            if Dict.get ( x, y ) trees == Just True then
                n + 1

            else
                n


input1 =
    [ "..##......."
    , "#...#...#.."
    , ".#....#..#."
    , "..#.#...#.#"
    , ".#...##..#."
    , "..#.##....."
    , ".#.#.#....#"
    , ".#........#"
    , "#.##...#..."
    , "#...##....#"
    , ".#..#...#.#"
    ]
