module Day17 exposing (..)

import Set exposing (Set)


solve : String -> Maybe ( Int, Int )
solve input =
    Just
        ( (part1 << d3 << parse) input
        , (part2 << d4 << parse) input
        )


parse : String -> List ( Int, Int )
parse input =
    String.lines input
        |> List.indexedMap
            (\y ->
                String.toList
                    >> List.indexedMap
                        (\x char ->
                            if char == '#' then
                                Just ( x, y )

                            else
                                Nothing
                        )
            )
        |> List.concat
        |> List.filterMap identity


part1 : Set D3 -> Int
part1 cubes =
    let
        tick a =
            cycle a d3neighbours

        initialNeighBours =
            Set.fromList (List.concatMap d3neighbours (Set.toList cubes))
    in
    repeat 6 tick ( cubes, initialNeighBours )
        |> Tuple.first
        |> Set.size


part2 : Set D4 -> Int
part2 cubes =
    let
        tick a =
            cycle a d4neighbours

        initialNeighBours =
            Set.fromList (List.concatMap d4neighbours (Set.toList cubes))
    in
    repeat 6 tick ( cubes, initialNeighBours )
        |> Tuple.first
        |> Set.size


cycle : ( Set comparable, Set comparable ) -> (comparable -> List comparable) -> ( Set comparable, Set comparable )
cycle ( cubes, toCheck ) neighbours =
    let
        f a acc =
            let
                neighboursOfA =
                    neighbours a

                count =
                    List.foldl countNeighbour 0 neighboursOfA
            in
            if count == 3 || (count == 2 && Set.member a cubes) then
                activate a acc neighboursOfA
            else 
                acc

        activate a ( accCubes, accToCheck ) neighboursOfA =
            ( Set.insert a accCubes
            , List.foldl Set.insert accToCheck neighboursOfA
            )

        countNeighbour neighbour count =
            if Set.member neighbour cubes then
                count + 1

            else
                count
    in
    Set.foldl f ( Set.empty, Set.empty ) toCheck


type alias D3 =
    ( Int, Int, Int )


d3 : List ( Int, Int ) -> Set D3
d3 =
    Set.fromList << List.map (\( x, y ) -> ( x, y, 0 ))


d3neighbours : D3 -> List D3
d3neighbours ( x, y, z ) =
    d3range ( x - 1, y - 1, z - 1 ) ( x + 1, y + 1, z + 1 ) ( x - 1, y - 1, z - 1 ) []
        |> List.filter ((/=) ( x, y, z ))


d3range : D3 -> D3 -> D3 -> List D3 -> List D3
d3range (( x_min, y_min, z_min ) as min) (( x_max, y_max, z_max ) as max) ( x, y, z ) acc =
    if z /= z_max then
        d3range min max ( x, y, z + 1 ) (( x, y, z ) :: acc)

    else if y /= y_max then
        d3range min max ( x, y + 1, z_min ) (( x, y, z ) :: acc)

    else if x /= x_max then
        d3range min max ( x + 1, y_min, z_min ) (( x, y, z ) :: acc)

    else
        ( x, y, z ) :: acc


type alias D4 =
    ( ( Int, Int ), ( Int, Int ) )


d4 : List ( Int, Int ) -> Set D4
d4 =
    Set.fromList << List.map (\( x, y ) -> ( ( x, y ), ( 0, 0 ) ))


d4neighbours : D4 -> List D4
d4neighbours ( ( x, y ), ( z, w ) ) =
    d4range ( ( x - 1, y - 1 ), ( z - 1, w - 1 ) ) ( ( x + 1, y + 1 ), ( z + 1, w + 1 ) ) ( ( x - 1, y - 1 ), ( z - 1, w - 1 ) ) []
        |> List.filter ((/=) ( ( x, y ), ( z, w ) ))


d4range : D4 -> D4 -> D4 -> List D4 -> List D4
d4range (( ( x_min, y_min ), ( z_min, w_min ) ) as min) (( ( x_max, y_max ), ( z_max, w_max ) ) as max) (( ( x, y ), ( z, w ) ) as point) acc =
    if w /= w_max then
        d4range min max ( ( x, y ), ( z, w + 1 ) ) (point :: acc)

    else if z /= z_max then
        d4range min max ( ( x, y ), ( z + 1, w_min ) ) (point :: acc)

    else if y /= y_max then
        d4range min max ( ( x, y + 1 ), ( z_min, w_min ) ) (point :: acc)

    else if x /= x_max then
        d4range min max ( ( x + 1, y_min ), ( z_min, w_min ) ) (point :: acc)

    else
        point :: acc


repeat : Int -> (a -> a) -> a -> a
repeat t f x =
    if t == 0 then
        x

    else
        repeat (t - 1) f (f x)
