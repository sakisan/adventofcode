module Day10 exposing (..)

import Dict
import Dict.Extra
import List.Extra
import Maybe.Extra


solve : String -> Maybe ( Int, Int )
solve input =
    String.lines input
        |> List.map String.toInt
        |> Maybe.Extra.combine
        |> Maybe.map List.sort
        |> Maybe.andThen
            (\list ->
                Maybe.map2 Tuple.pair
                    (part1 (0 :: list))
                    (part2 [ ( 1, 0 ) ] list)
            )


part1 : List Int -> Maybe Int
part1 numbers =
    case numbers of
        [] ->
            Nothing

        x :: xs ->
            let
                frequencies =
                    List.map2 (-) xs numbers
                        |> Dict.Extra.frequencies

                ones =
                    Dict.get 1 frequencies |> Maybe.withDefault 1

                threes =
                    Dict.get 3 frequencies |> Maybe.map ((+) 1) |> Maybe.withDefault 1
            in
            Just (ones * threes)


part2 : List ( Int, Int ) -> List Int -> Maybe Int
part2 cache numbers =
    case numbers of
        [] ->
            Maybe.map Tuple.first (List.head cache)

        x :: xs ->
            let
                count =
                    cache
                        |> List.Extra.takeWhile (\( c, n ) -> x - n <= 3)
                        |> List.map Tuple.first
                        |> List.sum
            in
            part2 (( count, x ) :: cache) xs
