module Day08 exposing (..)

import IntDict exposing (IntDict)
import Maybe.Extra


solve : String -> Maybe ( Int, Int )
solve input =
    readInstructions input
        -- |> Maybe.map (\instructions -> ( part1 instructions, -1 ))
        |> Maybe.map (\instructions -> ( part1 instructions, part2 0 instructions ))



-- CUSTOM TYPE


type BoundedIntDict a
    = BoundedIntDict ( Int, Int ) (IntDict a)


type Query a
    = Found a
    | NotFound
    | OutOfBounds


get : Int -> BoundedIntDict a -> Query a
get index dict =
    case dict of
        BoundedIntDict ( low, high ) intdict ->
            if low > index || high < index then
                OutOfBounds

            else
                case IntDict.get index intdict of
                    Nothing ->
                        NotFound

                    Just a ->
                        Found a


remove : Int -> BoundedIntDict a -> BoundedIntDict a
remove index dict =
    case dict of
        BoundedIntDict bounds intdict ->
            -- note: keep the original bounds
            BoundedIntDict bounds
                (IntDict.remove index intdict)


pop : Int -> BoundedIntDict a -> ( Query a, BoundedIntDict a )
pop index dict =
    case get index dict of
        NotFound ->
            ( NotFound, dict )

        OutOfBounds ->
            ( OutOfBounds, dict )

        Found a ->
            ( Found a, remove index dict )


update : Int -> (Maybe v -> Maybe v) -> BoundedIntDict v -> BoundedIntDict v
update index f (BoundedIntDict bounds dict) =
    BoundedIntDict bounds (IntDict.update index f dict)


empty : BoundedIntDict a
empty =
    BoundedIntDict ( 1, 0 ) IntDict.empty


size : BoundedIntDict a -> Int
size (BoundedIntDict _ dict) =
    IntDict.size dict


fromList : List ( Int, a ) -> BoundedIntDict a
fromList list =
    let
        f ( index, instruction ) acc =
            case acc of
                Just ( ( low, high ), dict ) ->
                    Just
                        ( ( min low index, max high index )
                        , IntDict.insert index instruction dict
                        )

                Nothing ->
                    Just
                        ( ( index, index )
                        , IntDict.singleton index instruction
                        )
    in
    List.foldl f Nothing list
        |> Maybe.map (uncurry BoundedIntDict)
        |> Maybe.withDefault empty


uncurry : (a -> b -> c) -> ( a, b ) -> c
uncurry f ( a, b ) =
    f a b



-- PROBLEM SOLVING


type alias Instructions =
    BoundedIntDict Instruction


type Instruction
    = Nop Int
    | Jmp Int
    | Acc Int


type alias Console =
    { accumulator : Int
    , offset : Int
    }


type Termination
    = InfiniteLoop Int
    | Terminated Int


readInstructions : String -> Maybe Instructions
readInstructions input =
    input
        |> String.lines
        |> List.map readInstruction
        |> Maybe.Extra.combine
        |> Maybe.map (List.indexedMap Tuple.pair >> fromList)


readInstruction : String -> Maybe Instruction
readInstruction line =
    case String.split " " line of
        [ "nop", argument ] ->
            Maybe.map Nop (String.toInt argument)

        [ "jmp", argument ] ->
            Maybe.map Jmp (String.toInt argument)

        [ "acc", argument ] ->
            Maybe.map Acc (String.toInt argument)

        _ ->
            Nothing


part1 : Instructions -> Int
part1 instructions =
    case run instructions (Console 0 0) of
        InfiniteLoop a ->
            a

        Terminated a ->
            -- Debug.todo "part1 does not terminate"
            -1


part2 : Int -> Instructions -> Int
part2 toggleIndex instructions =
    if toggleIndex > size instructions then
        -1

    else
        case run (toggle toggleIndex instructions) (Console 0 0) of
            InfiniteLoop a ->
                part2 (toggleIndex + 1) instructions

            Terminated a ->
                a


run : Instructions -> Console -> Termination
run instructions { accumulator, offset } =
    let
        -- _ =
        --     Debug.log "" ( size instructions, ( accumulator, offset ), termination )
        ( termination, newInstructions ) =
            pop offset instructions
    in
    case termination of
        OutOfBounds ->
            Terminated accumulator

        NotFound ->
            InfiniteLoop accumulator

        Found instruction ->
            case instruction of
                Nop _ ->
                    run newInstructions (Console accumulator (offset + 1))

                Jmp a ->
                    run newInstructions (Console accumulator (offset + a))

                Acc a ->
                    run newInstructions (Console (accumulator + a) (offset + 1))


toggle : Int -> Instructions -> Instructions
toggle index instructions =
    let
        t present =
            case present of
                Nop a ->
                    Jmp a

                Jmp a ->
                    Nop a

                Acc a ->
                    Acc a
    in
    update index (Maybe.map t) instructions


input1 =
    """
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
"""
