module Day15 exposing (..)

import Dict exposing (Dict)
import List.Extra
import Maybe.Extra


solve : String -> Maybe ( Int, Int )
solve input =
    String.split "," input
        |> List.map String.toInt
        |> Maybe.Extra.combine
        |> Maybe.andThen (\list -> Maybe.map2 Tuple.pair (part1 list) (part2 list))


part1 : List Int -> Maybe Int
part1 list =
    setup 2021 list


part2 : List Int -> Maybe Int
part2 list =
    setup 30000001 list


type alias History =
    Dict Int Int


setup : Int -> List Int -> Maybe Int
setup target list =
    case List.head (List.reverse list) of
        Nothing ->
            Nothing

        Just last ->
            Just <| run target (List.length list + 1) last (init list)


init : List Int -> History
init list =
    List.map2 Tuple.pair
        list
        (List.range 1 (List.length list))
        |> Dict.fromList


run : Int -> Int -> Int -> History -> Int
run target turn previous history =
    let
        -- _ = Debug.log "" ( turn, previous )
        next =
            case Dict.get previous history of
                Nothing ->
                    0

                Just p2 ->
                    turn - p2 - 1

        updatedHistory =
            Dict.insert previous (turn - 1) history
    in
    if target == turn then
        previous

    else
        run target (turn + 1) next updatedHistory
