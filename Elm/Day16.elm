module Day16 exposing (..)

import List.Extra 
import Parser exposing ((|.), (|=), Parser, Trailing(..))

solve : String -> Maybe (Int,Int)
solve input = 
    parse input
    |> Maybe.map (\i -> (part1 i, part2 i))


type alias Rule = 
    { name : String
    , range1 : (Int, Int)
    , range2 : (Int, Int)
    }


type alias Input =
    { rules : List Rule
    , myTicket : List Int
    , nearbyTickets : List (List Int)
    }


part1 : Input -> Int
part1 {rules, nearbyTickets} = 
    let
        invalidInts = List.filterMap invalid
        invalid n = if List.any (pass n) rules then Nothing else Just n
    in
    List.sum (List.concatMap invalidInts nearbyTickets)


pass : Int -> Rule -> Bool
pass n {range1, range2} =
    inRange range1 n || inRange range2 n


inRange : (Int, Int) -> Int -> Bool
inRange (low, high) n = 
    low <= n && n <= high


part2 : Input -> Int
part2 {rules, myTicket, nearbyTickets} =
    let
        -- discard invalid tickets
        validTickets : List (List Int)
        validTickets = List.filter (validTicket rules) nearbyTickets

        -- Group numbers by their position (field)
        numberLists : List (List Int)
        numberLists = List.Extra.transpose validTickets

        -- Clone rules for each field
        rulesPerField : List (List Rule)
        rulesPerField = List.map (always rules) numberLists

        -- Remove rules which can't match all numbers
        leftOverRules : List (List Rule)
        leftOverRules = List.map2 eliminateRules numberLists rulesPerField

        -- Figure out which rule matches which field
        selectedRules : List (Int, Rule)
        selectedRules = shake leftOverRules []

        -- Select numbers for the departure rules
        departureValues : List Int
        departureValues = 
            List.filterMap (\(i, {name}) -> 
                if String.startsWith "departure" name then 
                    List.Extra.getAt i myTicket
                else 
                    Nothing
                ) 
                selectedRules
    in
    List.product departureValues

shake : List (List Rule) -> List (Int, Rule) -> List (Int, Rule)
shake rulesPerField result =
    let
        -- Select rules which are the only possibility left for a field
        selectedRules : List (Int, Rule)
        selectedRules = 
            rulesPerField
            |> List.map2 Tuple.pair (List.range 0 (List.length rulesPerField))
            |> List.filter (\(_,rules) -> List.length rules == 1) 
            |> List.filterMap (\(i,rules) -> Maybe.map (\rule -> (i,rule)) (List.head rules))

        rulesToRemove : List Rule
        rulesToRemove = List.map Tuple.second selectedRules

        -- Remove those selected rules from all fields
        newRulesPerField : List (List Rule)
        newRulesPerField = 
            List.map (removeAll rulesToRemove) rulesPerField

        newResult : List (Int, Rule)
        newResult = result ++ selectedRules 
    in
    if [] == selectedRules then
        newResult
    else
        shake newRulesPerField newResult


validTicket : List Rule -> List Int -> Bool
validTicket rules ticket = 
    List.all (\n -> List.any (pass n) rules) ticket


eliminateRules : List Int -> List Rule -> List Rule
eliminateRules numbers rules =
    List.filter (\rule -> List.all (\n -> pass n rule) numbers) rules


removeAll : List a -> List a -> List a
removeAll toRemove list =
    List.foldl List.Extra.remove list toRemove



-- PARSER

parse : String -> Maybe Input
parse input =
    Parser.run inputParser input
        |> Result.toMaybe
    

inputParser : Parser Input
inputParser =
    Parser.succeed Input
        |= ruleParser
        |. Parser.spaces
        |. Parser.keyword "your ticket:"
        |. Parser.spaces
        |= intsParser
        |. Parser.spaces
        |. Parser.keyword "nearby tickets:"
        |. Parser.spaces
        |= Parser.sequence
            { start = ""
            , separator = "\n"
            , end = ""
            , spaces = Parser.succeed ()
            , item = intsParser
            , trailing = Optional
            }



ruleParser : Parser (List Rule)
ruleParser =
    let
        parser : Parser Rule
        parser =
            Parser.succeed Rule
                |= Parser.getChompedString (Parser.chompUntil ": ")
                |. Parser.symbol ": "
                |= rangeParser
                |. Parser.symbol " or "
                |= rangeParser
    in
    Parser.sequence
        { start = ""
        , separator = "\n"
        , end = ""
        , spaces = Parser.succeed ()
        , item = parser
        , trailing = Optional
        }


rangeParser : Parser ( Int, Int )
rangeParser =
    Parser.succeed Tuple.pair
        |= Parser.int
        |. Parser.symbol "-"
        |= Parser.int


intsParser : Parser (List Int)
intsParser =
    Parser.sequence
        { start = ""
        , separator = ","
        , end = ""
        , spaces = Parser.succeed ()
        , item = Parser.int
        , trailing = Optional
        }


input1 = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12"""


