module Main exposing (..)

import Browser
import Day01_Optimized as Day01 exposing (solve)
import Day02 exposing (solve)
import Day03 exposing (solve)
import Day04 exposing (solve)
import Day05 exposing (solve)
import Day06 exposing (solve)
import Day07 exposing (solve)
import Day08 exposing (solve)
import Day09 exposing (solve)
import Day10 exposing (solve)
import Day11 exposing (solve)
import Day12 exposing (solve)
import Day13 exposing (solve)
import Day14 exposing (solve)
import Day15 exposing (solve)
import Day16 exposing (solve)
import Day17 exposing (solve)
import Day18 exposing (solve)
import Day19 exposing (solve)
import Day21 exposing (solve)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Events exposing (onClick, onInput)
import List.Extra
import Style exposing (..)
import Task exposing (Task)
import Time exposing (Posix)


main : Program () Model Msg
main =
    Browser.document
        { init = always ( initialModel, Cmd.none )
        , update = update
        , view = \model -> { title = "AoC 2020  sakisan.be", body = [ view model ] }
        , subscriptions = always Sub.none
        }


type alias Model =
    { input : String
    , lastButtonClicked : Maybe Day
    , solved : Maybe ( String, String )
    , tracking : Maybe ( Day, Posix )
    , time : Maybe Int
    }


initialModel =
    { input = ""
    , lastButtonClicked = Nothing
    , solved = Nothing
    , tracking = Nothing
    , time = Nothing
    }


type Day
    = Day01
    | Day02
    | Day03
    | Day04
    | Day05
    | Day06
    | Day07
    | Day08
    | Day09
    | Day10
    | Day11
    | Day12
    | Day13
    | Day14
    | Day15
    | Day16
    | Day17
    | Day18
    | Day19
    | Day21


type Msg
    = InsertText String
    | Solve Day
    | StartTracking Day Posix
    | StopTracking Day ( Posix, Maybe ( String, String ) )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Solve day ->
            ( { model | lastButtonClicked = Just day }, startTracking model day )

        InsertText text ->
            ( { model | input = text, lastButtonClicked = Nothing }, Cmd.none )

        StartTracking day posix ->
            ( { model | tracking = Just ( day, posix ) }, startSolving day (trim model.input) )

        StopTracking day ( end, result ) ->
            ( case model.tracking of
                Nothing ->
                    model

                Just ( d, start ) ->
                    if d /= day then
                        model

                    else
                        { model
                            | solved = result
                            , tracking = Nothing
                            , time = Just (Time.posixToMillis end - Time.posixToMillis start)
                        }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    Element.layout [ Background.color backgroundColor, Font.color fontColor ] <|
        Element.column
            [ fillWidth, Element.spacing 15, Element.padding 10 ]
            [ Element.el [ Region.heading 1, Font.size 36 ] (Element.text "Advent of Code 2020")
            , Element.row [ Element.spacing 20 ] (viewAnswers model ++ timeSpent model)
            , Element.wrappedRow [ Element.spacing 10 ]
                [ viewButton model Day01 "Day 1"
                , viewButton model Day02 "Day 2"
                , viewButton model Day03 "Day 3"
                , viewButton model Day04 "Day 4"
                , viewButton model Day05 "Day 5"
                , viewButton model Day06 "Day 6"
                , viewButton model Day07 "Day 7"
                , viewButton model Day08 "Day 8"
                , viewButton model Day09 "Day 9"
                , viewButton model Day10 "Day 10"
                , viewButton model Day11 "Day 11"
                , viewButton model Day12 "Day 12"
                , viewButton model Day13 "Day 13"
                , viewButton model Day14 "Day 14"
                , viewButton model Day15 "Day 15"
                , viewButton model Day16 "Day 16"
                , viewButton model Day17 "Day 17"
                , viewButton model Day18 "Day 18"
                , viewButton model Day19 "Day 19"
                , viewButton model Day21 "Day 21"
                ]
            , Input.multiline
                [ Element.height (Element.px 300)
                , Background.color backgroundColor
                ]
                { onChange = InsertText
                , text = model.input
                , placeholder = Nothing
                , label = Input.labelAbove [] (Element.text "Input")
                , spellcheck = False
                }
            , Element.link [ Font.underline ]
                { url = "https://gitlab.com/sakisan/adventofcode/-/tree/2020"
                , label = Element.text "source code"
                }
            ]


viewButton : Model -> Day -> String -> Element Msg
viewButton model day label =
    let
        active =
            model.lastButtonClicked == Just day
    in
    Input.button
        [ Background.color
            (if active then
                buttonColorActive

             else
                buttonColor
            )
        , Border.color
            (if active then
                buttonColorActive

             else
                backgroundColor
            )
        , Border.width 5
        , Border.rounded 10
        , Element.padding 10
        ]
        { onPress = Just (Solve day)
        , label = Element.text label
        }


viewAnswers : Model -> List (Element Msg)
viewAnswers model =
    case model.solved of
        Just ( part1, part2 ) ->
            viewParts part1 part2

        Nothing ->
            case model.lastButtonClicked of
                Nothing ->
                    viewParts "" ""

                Just _ ->
                    [ Element.text "Invalid input" ]


viewParts : String -> String -> List (Element Msg)
viewParts part1 part2 =
    [ viewPart "Part 1:" part1
    , viewPart "Part 2:" part2
    ]


viewPart : String -> String -> Element msg
viewPart label answer =
    Element.row [ Element.spacing 5 ]
        [ Element.text label
        , Element.el [ Font.color yellow ] (Element.text answer)
        ]


timeSpent : Model -> List (Element Msg)
timeSpent model =
    case model.time of
        Nothing ->
            []

        Just time ->
            [ Element.row [ Element.spacing 5 ]
                [ Element.text "Time spent:"
                , Element.el [ Font.color yellow ]
                    (if time < 60000 then
                        Element.row []
                            [ Element.text (String.fromInt time)
                            , Element.el [] (Element.text "ms")
                            ]

                     else
                        Element.row [ Element.spacing 5 ]
                            [ Element.row []
                                [ Element.text (String.fromInt (time // 60000))
                                , Element.el [] (Element.text "m")
                                ]
                            , Element.row []
                                [ Element.text (String.fromInt ((time |> modBy 60000) // 1000))
                                , Element.el [] (Element.text "s")
                                ]
                            , Element.row []
                                [ Element.text (String.fromInt (time |> modBy 1000))
                                , Element.el [] (Element.text "ms")
                                ]
                            ]
                    )
                ]
            ]


startTracking : Model -> Day -> Cmd Msg
startTracking model day =
    let
        task =
            Task.perform (StartTracking day) Time.now
    in
    case model.tracking of
        Nothing ->
            task

        Just ( d, time ) ->
            if d == day then
                Cmd.none

            else
                task


startSolving : Day -> String -> Cmd Msg
startSolving day input =
    let
        result =
            solve day input
    in
    Task.perform (StopTracking day) <|
        Task.map (\time -> ( time, result )) Time.now


solve : Day -> String -> Maybe ( String, String )
solve day =
    let
        twoInts =
            Maybe.map (\( i1, i2 ) -> ( String.fromInt i1, String.fromInt i2 ))

        oneInt =
            Maybe.map (\( i1, i2 ) -> ( String.fromInt i1, i2 ))
    in
    case day of
        Day01 ->
            Day01.solve >> twoInts

        Day02 ->
            Day02.solve >> twoInts

        Day03 ->
            Day03.solve >> twoInts

        Day04 ->
            Day04.solve >> twoInts

        Day05 ->
            Day05.solve >> twoInts

        Day06 ->
            Day06.solve >> twoInts

        Day07 ->
            Day07.solve >> twoInts

        Day08 ->
            Day08.solve >> twoInts

        Day09 ->
            Day09.solve >> twoInts

        Day10 ->
            Day10.solve >> twoInts

        Day11 ->
            Day11.solve >> twoInts

        Day12 ->
            Day12.solve >> twoInts

        Day13 ->
            Day13.solve >> twoInts

        Day14 ->
            Day14.solve >> twoInts

        Day15 ->
            Day15.solve >> twoInts

        Day16 ->
            Day16.solve >> twoInts

        Day17 ->
            Day17.solve >> twoInts

        Day18 ->
            Day18.solve >> twoInts

        Day19 ->
            Day19.solve >> twoInts

        Day21 ->
            Day21.solve >> oneInt


trim : String -> String
trim input =
    input
        |> String.lines
        |> List.Extra.dropWhile (\line -> String.length line == 0)
        |> List.reverse
        |> List.Extra.dropWhile (\line -> String.length line == 0)
        |> List.reverse
        |> String.join "\n"
