module Day09 exposing (..)

import Array exposing (Array)
import Day01_Optimized exposing (search)
import Maybe.Extra


solve : String -> Maybe ( Int, Int )
solve input =
    String.lines input
        |> List.map String.toInt
        |> Maybe.Extra.combine
        |> Maybe.andThen solve_


preamble : Int
preamble =
    25


solve_ : List Int -> Maybe ( Int, Int )
solve_ list =
    let
        array =
            Array.fromList list
    in
    part1 preamble array
        |> Maybe.andThen
            (\num1 ->
                Maybe.map
                    (\num2 ->
                        ( num1, num2 )
                    )
                    (part2 num1 array 0 2)
            )


part1 : Int -> Array Int -> Maybe Int
part1 index array =
    let
        end =
            Array.length array
    in
    if index > end then
        Nothing

    else if isValid array index then
        part1 (index + 1) array

    else
        Array.get index array


isValid : Array Int -> Int -> Bool
isValid array index =
    let
        sortedSlice =
            Array.slice (index - preamble) index array
                |> Array.toList
                |> List.sort
                |> Array.fromList
    in
    Array.get index array
        |> Maybe.andThen (\target -> search sortedSlice target 0 (preamble - 1))
        |> Maybe.Extra.isJust


part2 : Int -> Array Int -> Int -> Int -> Maybe Int
part2 num1 array from to =
    let
        end =
            Array.length array

        slice =
            Array.toList (Array.slice from to array)

        sum =
            List.sum slice
    in
    if to > end then
        Nothing

    else if sum == num1 then
        Maybe.map2 (+)
            (List.minimum slice)
            (List.maximum slice)

    else if sum < num1 then
        part2 num1 array from (to + 1)

    else if from == to - 2 then
        part2 num1 array (from + 1) (to + 1)

    else
        part2 num1 array (from + 1) to
