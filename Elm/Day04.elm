module Day04 exposing (..)

import Parser exposing (..)


solve : String -> Maybe ( Int, Int )
solve input =
    Result.map2 Tuple.pair
        (run part1 input)
        (run part2 input)
        -- (Ok -1)
        |> Result.toMaybe


run : Parser () -> String -> Result (List DeadEnd) Int
run passportParser input =
    Parser.run (iteratePassports 0 passportParser) (input ++ "\n\n")


iteratePassports : Int -> Parser () -> Parser Int
iteratePassports count validPassportParser =
    let
        -- _ = Debug.log "count" count
        increment : () -> Parser Int
        increment _ =
            iteratePassports (count + 1) validPassportParser

        continue : () -> Parser Int
        continue _ =
            iteratePassports count validPassportParser
    in
    oneOf
        [ backtrackable validPassportParser |> andThen increment
        , emptyLine |> andThen continue
        , invalidPassportParser |> andThen continue
        , end |> Parser.map (always count)
        ]


emptyLine : Parser ()
emptyLine =
    chompIf ((==) '\n')
        |> failIfNothingChomped


invalidPassportParser : Parser ()
invalidPassportParser =
    chompUntil "\n\n"
        |> failIfNothingChomped


failIfNothingChomped : Parser () -> Parser ()
failIfNothingChomped parser =
    getChompedString parser
        |> andThen
            (\str ->
                if String.length str == 0 then
                    problem "nothing chomped"

                else
                    succeed ()
            )


isFieldSeparator : Char -> Bool
isFieldSeparator c =
    c == ' ' || c == '\n'


fieldSeparator : Parser ()
fieldSeparator =
    chompIf isFieldSeparator


fieldName : Parser String
fieldName =
    getChompedString (chompWhile Char.isAlpha)
        |. symbol ":"


ignoreValue : Parser ()
ignoreValue =
    chompWhile (not << isFieldSeparator)


type alias NamedParser =
    ( String, Parser () )


fieldParser : Parser a -> String -> NamedParser
fieldParser valueParser name =
    fieldName
        |. valueParser
        -- |. debug name
        |> andThen
            (\parsedName ->
                if name == parsedName then
                    succeed ()

                else
                    problem "w/e"
            )
        |> Tuple.pair name


processFields : List NamedParser -> Parser ()
processFields parsers =
    -- let _ = Debug.log "" (List.map Tuple.first parsers) in
    case parsers of
        -- no fields left, that means all fields have succesfully parsed
        [] ->
            succeed ()

        -- one field left, but cid is optional so it gets a pass
        [ ( "cid", lastParser ) ] ->
            succeed ()

        -- handle the last field here,
        -- so in the last branch we know we always have more fields to try
        [ ( name, lastParser ) ] ->
            lastParser

        -- |. debug name
        -- multiple fields left
        -- try a field and if it works,
        --      remove it from the available fields then continue (recurse)
        _ ->
            oneOf <|
                List.map
                    (\( name, parser ) ->
                        let
                            otherName ( name_, _ ) =
                                name /= name_

                            otherParsers =
                                List.filter otherName parsers

                            -- _ = Debug.log "" name
                        in
                        parser
                            |. fieldSeparator
                            -- |. debug name
                            |> backtrackable
                            |> andThen (\_ -> processFields otherParsers)
                    )
                    parsers


part1 : Parser ()
part1 =
    processFields
        [ fieldParser ignoreValue "byr"
        , fieldParser ignoreValue "iyr"
        , fieldParser ignoreValue "eyr"
        , fieldParser ignoreValue "hgt"
        , fieldParser ignoreValue "hcl"
        , fieldParser ignoreValue "ecl"
        , fieldParser ignoreValue "pid"
        , fieldParser ignoreValue "cid"
        ]


part2 : Parser ()
part2 =
    processFields
        [ fieldParser (validIf (between 1920 2002) int) "byr"
        , fieldParser (validIf (between 2010 2020) int) "iyr"
        , fieldParser (validIf (between 2020 2030) int) "eyr"
        , fieldParser
            (validIf
                (\( height, unit ) ->
                    case unit of
                        "cm" ->
                            between 150 193 height

                        "in" ->
                            between 59 76 height

                        _ ->
                            False
                )
                (succeed Tuple.pair
                    |= int
                    |= remainingValue
                )
            )
            "hgt"
        , fieldParser (symbol "#" |. hex |. hex |. hex |. hex |. hex |. hex) "hcl"
        , fieldParser (validIfIn [ "amb", "blu", "brn", "gry", "grn", "hzl", "oth" ] remainingValue) "ecl"
        , fieldParser
            (validIf
                (\str -> String.length str == 9 && String.toInt str /= Nothing)
                remainingValue
            )
            "pid"
        , fieldParser ignoreValue "cid"
        ]


validIf : (a -> Bool) -> Parser a -> Parser a
validIf validate =
    andThen
        (\x ->
            -- let _ = Debug.log "" x in
            if validate x then
                succeed x

            else
                problem "invalid"
        )


validIfIn : List a -> Parser a -> Parser a
validIfIn validValues =
    validIf (\x -> List.member x validValues)


chompOne : Parser Char
chompOne =
    getChompedString (chompIf (always True))
        |> andThen
            (\str ->
                case String.uncons str of
                    Nothing ->
                        problem ""

                    Just ( x, _ ) ->
                        succeed x
            )


remainingValue : Parser String
remainingValue =
    getChompedString (chompWhile Char.isAlphaNum)


hex : Parser Char
hex =
    validIfIn (String.toList "0123456789abcdef") chompOne


between : Int -> Int -> Int -> Bool
between low high value =
    low <= value && value <= high


debug name =
    succeed
        (\i source ->
            -- Debug.log name 
                (String.left i source)
        )
        |= getOffset
        |= getSource


valid =
    """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""


invalid =
    """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007
"""


mixed =
    """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""
