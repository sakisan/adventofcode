module Day07 exposing (..)

import Dict exposing (Dict)
import Graph exposing (Edge, Graph, Node, NodeContext)
import IntDict exposing (IntDict)
import Parser exposing ((|.), (|=), Parser, Trailing(..))
import Set exposing (Set)


solve : String -> Maybe ( Int, Int )
solve input =
    Parser.run parser input
        |> Result.toMaybe
        |> Maybe.andThen toGraph
        |> Maybe.map (\graph -> ( part1 graph, part2 graph ))



-- PARSER


parser : Parser (List ( String, List ( Int, String ) ))
parser =
    Parser.sequence
        { start = ""
        , separator = ""
        , end = ""
        , spaces = Parser.spaces
        , item = lineParser
        , trailing = Optional
        }


lineParser : Parser ( String, List ( Int, String ) )
lineParser =
    Parser.succeed Tuple.pair
        |= bagParser
        |. Parser.spaces
        |. Parser.token "contain"
        |. Parser.spaces
        |= Parser.oneOf
            [ Parser.succeed []
                |. Parser.token "no other bags."
            , Parser.sequence
                { start = ""
                , separator = ","
                , end = ""
                , spaces = Parser.chompWhile ((==) ' ')
                , item = nBagsParser
                , trailing = Forbidden
                }
            ]


bagParser : Parser String
bagParser =
    Parser.succeed (\a b -> a ++ " " ++ b)
        |= Parser.getChompedString (Parser.chompWhile Char.isAlpha)
        |. Parser.spaces
        |= Parser.getChompedString (Parser.chompWhile Char.isAlpha)
        |. Parser.spaces
        |. Parser.oneOf [ Parser.token "bags", Parser.token "bag" ]
        |. Parser.oneOf [ Parser.symbol ".", Parser.succeed () ]


nBagsParser : Parser ( Int, String )
nBagsParser =
    Parser.succeed Tuple.pair
        |= Parser.int
        |. Parser.spaces
        |= bagParser


debug name =
    Parser.succeed
        (\i source ->
            -- Debug.log name 
                (String.left i source)
        )
        |= Parser.getOffset
        |= Parser.getSource



-- GRAPH


toGraph : List ( String, List ( Int, String ) ) -> Maybe ( Int, Graph String Int )
toGraph list =
    let
        nodeIds : Dict String Int
        nodeIds =
            List.indexedMap (\i ( bag, _ ) -> ( bag, i )) list
                |> Dict.fromList

        nodes : List (Node String)
        nodes =
            Dict.toList nodeIds
                |> List.map (\( bag, i ) -> Node i bag)

        edges : List (Edge Int)
        edges =
            List.foldl
                (\( bag, containedBags ) acc ->
                    acc
                        ++ List.map
                            (\( n, containedBag ) ->
                                Maybe.map3 Edge
                                    (Dict.get bag nodeIds)
                                    (Dict.get containedBag nodeIds)
                                    (Just n)
                            )
                            containedBags
                )
                []
                list
                |> List.filterMap identity
    in
    Dict.get "shiny gold" nodeIds
        |> Maybe.map (\goldId -> ( goldId, Graph.fromNodesAndEdges nodes edges ))


part1 : ( Int, Graph String Int ) -> Int
part1 ( goldId, graph ) =
    graph
        |> Graph.guidedBfs
            Graph.alongIncomingEdges
            (Graph.ignorePath (\nodeContext acc -> Set.insert nodeContext.node.id acc))
            [ goldId ]
            Set.empty
        |> Tuple.first
        |> Set.size
        -- not counting the id for the gold bag
        |> (\size -> size - 1)



-- both the BFS and DFS functions do not visit the same nodes twice,
-- thus are useless for part 2


part2 : ( Int, Graph String Int ) -> Int
part2 ( goldId, graph ) =
    countChildren graph goldId - 1


countChildren : Graph String Int -> Int -> Int
countChildren graph id =
    case Graph.get id graph of
        Nothing ->
            0

        Just { outgoing } ->
            IntDict.foldl
                (\outgoingId n acc -> acc + n * countChildren graph outgoingId)
                1
                outgoing



-- INPUT


input1 =
    """
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
"""


input2 =
    """
shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.
"""
