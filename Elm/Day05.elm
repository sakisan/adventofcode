module Day05 exposing (..)

import Maybe.Extra exposing (combine, join)


solve : String -> Maybe ( Int, Int )
solve input =
    let
        seats =
            String.lines input
                |> List.map seatID
                |> combine

        part1 =
            seats
                |> Maybe.map List.maximum
                |> join

        part2 =
            seats
                |> Maybe.map (List.reverse << List.sort)
                |> Maybe.map2 findMissing part1
                |> join
    in
    Maybe.map2 Tuple.pair
        part1
        part2


seatID : String -> Maybe Int
seatID line =
    if String.length line /= 7 + 3 then
        Nothing

    else
        Maybe.map2 (\row col -> 8 * row + col)
            (toBinary 'B' 'F' (String.left 7 line))
            (toBinary 'R' 'L' (String.right 3 line))


toBinary : Char -> Char -> String -> Maybe Int
toBinary one zero str =
    let
        value c =
            if c == one then
                Just 1

            else if c == zero then
                Just 0

            else
                Nothing

        fold : Char -> Maybe Int -> Maybe Int
        fold c num =
            Maybe.map2 (\v sum -> 2 * sum + v)
                (value c)
                num
    in
    List.foldl fold (Just 0) (String.toList str)


findMissing : Int -> List Int -> Maybe Int
findMissing check list =
    case list of
        [] ->
            Nothing

        seat :: lowerSeats ->
            if seat == check then
                findMissing (check - 1) lowerSeats

            else
                Just check
