module Day19 exposing (..)

import Dict exposing (Dict)
import List.Extra
import Maybe.Extra
import Parser exposing ((|.), (|=), Parser, Trailing(..))
import Set exposing (Set)


solve : String -> Maybe ( Int, Int )
solve input =
    let
        lines =
            String.lines input

        ( ruleLines, messageLines ) =
            List.Extra.span ((/=) "") lines

        messages =
            List.drop 1 messageLines
    in
    List.map parseRule ruleLines
        |> Maybe.Extra.combine
        |> Maybe.map Dict.fromList
        |> Maybe.map
            (\rules ->
                ( countValidMessages messages rules
                , countValidMessages messages
                    (rules
                        |> Dict.insert 8 (Or [ 42 ] [ 42, 8 ])
                        |> Dict.insert 11 (Or [ 42, 31 ] [ 42, 11, 31 ])
                    )
                )
            )


type Rule
    = Literal String
    | Sequence (List Int)
    | Or (List Int) (List Int)


type alias Rules =
    Dict Int Rule


countValidMessages : List String -> Rules -> Int
countValidMessages messages rules =
    -- let _ = Debug.log "" rules in
    List.length (List.filter (isValid rules) messages)


isValid : Rules -> String -> Bool
isValid rules message =
    Set.member "" (match rules 0 message)


match : Rules -> Int -> String -> Set String
match rules n string =
    -- let _ = Debug.log "" (n,string) in
    case string of
        "" ->
            Set.singleton ""

        _ ->
            case Dict.get n rules of
                Nothing ->
                    Debug.todo "invalid rule number"

                Just (Literal a) ->
                    if String.startsWith a string then
                        Set.singleton (String.dropLeft 1 string)

                    else
                        Set.empty

                Just (Sequence ns) ->
                    matchSequence rules ns string

                Just (Or ns1 ns2) ->
                    Set.union (matchSequence rules ns1 string) (matchSequence rules ns2 string)


matchSequence : Rules -> List Int -> String -> Set String
matchSequence rules ns string =
    case ( ns, string ) of
        ( [], "" ) ->
            Set.singleton ""

        ( [], _ ) ->
            Set.singleton string

        ( n :: moreNs, _ ) ->
            let
                matches =
                    match rules n string
            in
            if Set.isEmpty matches then
                Set.empty

            else if Set.member "" matches then
                case moreNs of
                    [] ->
                        Set.singleton ""

                    _ :: _ ->
                        Set.empty

            else
                matches
                    |> Set.toList
                    |> List.map (matchSequence rules moreNs)
                    |> List.foldl Set.union Set.empty


parseRule : String -> Maybe ( Int, Rule )
parseRule line =
    Parser.run numberedRuleParser line
        |> Result.toMaybe


numberedRuleParser : Parser ( Int, Rule )
numberedRuleParser =
    Parser.succeed Tuple.pair
        |= Parser.int
        |. Parser.symbol ":"
        |. Parser.spaces
        |= ruleParser


ruleParser : Parser Rule
ruleParser =
    -- the input is quite limited in its formats
    Parser.oneOf
        [ Parser.succeed (Literal "a") |. Parser.symbol "\"a\""
        , Parser.succeed (Literal "b") |. Parser.symbol "\"b\""
        , Parser.succeed (\a b c d -> Or [ a, b ] [ c, d ])
            |= Parser.int
            |. Parser.spaces
            |= Parser.int
            |. Parser.spaces
            |. Parser.symbol "|"
            |. Parser.spaces
            |= Parser.int
            |. Parser.spaces
            |= Parser.int
            |> Parser.backtrackable
        , Parser.succeed (\a b -> Or [ a ] [ b ])
            |= Parser.int
            |. Parser.spaces
            |. Parser.symbol "|"
            |. Parser.spaces
            |= Parser.int
            |> Parser.backtrackable
        , Parser.succeed (\a b c -> Sequence [ a, b, c ])
            |= Parser.int
            |. Parser.spaces
            |= Parser.int
            |. Parser.spaces
            |= Parser.int
            |> Parser.backtrackable
        , Parser.succeed (\a b -> Sequence [ a, b ])
            |= Parser.int
            |. Parser.spaces
            |= Parser.int
            |> Parser.backtrackable
        , Parser.succeed (\a -> Sequence [ a ])
            |= Parser.int
        ]
