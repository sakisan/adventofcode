module Day01_Optimized exposing (..)

import Array exposing (Array)


solve : String -> Maybe ( Int, Int )
solve input =
    let
        numbers =
            input
                |> String.lines
                |> List.filterMap String.toInt
                |> List.sort
                |> Array.fromList

        numberOfNumbers =
            Array.length numbers - 1

        part1 =
            search numbers 2020 0 numberOfNumbers
                |> Maybe.map (\( a, b ) -> a * b)

        part2 =
            search2 numbers 0 1 numberOfNumbers
    in
    Maybe.map2 Tuple.pair part1 part2



{-
   The array is sorted.
   Move 2 pointers, one from the start and one from the end,
   depending on whether the sum is < or > than 2020.
-}


search : Array Int -> Int -> Int -> Int -> Maybe ( Int, Int )
search array target low high =
    if low == high then
        Nothing

    else
        Maybe.map2 Tuple.pair
            (Array.get low array)
            (Array.get high array)
            |> Maybe.andThen
                (\( a, b ) ->
                    if a + b > target then
                        search array target low (high - 1)

                    else if a + b < target then
                        search array target (low + 1) high

                    else
                        Just ( a, b )
                )


search2 : Array Int -> Int -> Int -> Int -> Maybe Int
search2 array low low2 high =
    if low >= high - 2 then
        Nothing

    else
        case Array.get low array of
            Just a ->
                case search array (2020 - a) low2 high of
                    Just ( b, c ) ->
                        Just (a * b * c)

                    Nothing ->
                        search2 array (low + 1) (low + 2) high

            Nothing ->
                Nothing
