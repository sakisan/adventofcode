module Day18 exposing (..)

-- I did not use
-- https://package.elm-lang.org/packages/dmy/elm-pratt-parser/latest/
-- even though it's obviously the right tool for the job.
-- I wanted to practice implementing the precedence myself
-- do I regret it?
-- maybe...

import Maybe.Extra
import Parser exposing ((|.), (|=), Parser, Trailing(..))


solve : String -> Maybe ( Int, Int )
solve input =
    String.lines input
        |> List.map parseExpression
        |> Maybe.Extra.combine
        |> Maybe.map (fork part1 part2)


fork : (a -> b) -> (a -> c) -> a -> ( b, c )
fork f g a =
    ( f a, g a )


type Expression
    = Value Int
    | Level Expression (List ( Operator, Expression ))


type Operator
    = Addition
    | Multiplication


part1 : List Expression -> Int
part1 expressions =
    List.sum (List.map (unpack << evaluate [ [ Addition, Multiplication ] ]) expressions)


part2 : List Expression -> Int
part2 expressions =
    List.sum (List.map (unpack << evaluate [ [ Addition ], [ Multiplication ] ]) expressions)


evaluate : List (List Operator) -> Expression -> Expression
evaluate operators expression =
    let
        apply : (Int -> Int) -> Expression -> Expression
        apply operate e =
            case e of
                Value i ->
                    Value (operate i)

                Level x xs ->
                    case List.reverse xs of
                        [] ->
                            Value (operate (unpack (evaluate operators x)))

                        ( op, last ) :: more ->
                            Level x 
                                <| List.reverse
                                <| (\p -> p :: more)
                                <| Tuple.pair op
                                <| Value
                                <| operate
                                <| unpack
                                <| evaluate operators
                                <| last

        fo ops acc =
            case acc of
                Value i ->
                    Value i

                Level x xs ->
                    List.foldl (f ops) (evaluate operators x) xs

        f ops ( op, y ) acc =
            if List.member op ops then
                case op of
                    Addition ->
                        apply (\i -> i + unpack (evaluate operators y)) acc

                    Multiplication ->
                        apply (\i -> i * unpack (evaluate operators y)) acc

            else
                lift acc [ ( op, y ) ]
    in
    List.foldl fo expression operators


lift : Expression -> List ( Operator, Expression ) -> Expression
lift expression l =
    case expression of
        Value i ->
            Level (Value i) l

        Level e list ->
            Level e (list ++ l)


unpack : Expression -> Int
unpack expression =
    case expression of
        Value i ->
            i

        _ ->
            Debug.todo (Debug.toString expression)


parseExpression : String -> Maybe Expression
parseExpression line =
    Parser.run expressionParser line
        |> Result.toMaybe


expressionParser : Parser Expression
expressionParser =
    let
        remainderOfLevel e =
            Parser.oneOf
                [ levelParser e
                , Parser.succeed e
                ]
    in
    Parser.oneOf
        [ valueParser
            |. Parser.spaces
            |> Parser.andThen remainderOfLevel
        , Parser.succeed (\e -> Level e [])
            |. Parser.symbol "("
            |= Parser.lazy (\() -> expressionParser)
            |. Parser.symbol ")"
            |. Parser.spaces
            |> Parser.andThen remainderOfLevel
        ]


operatorParser : Parser Operator
operatorParser =
    Parser.oneOf
        [ Parser.succeed Addition |. Parser.symbol "+"
        , Parser.succeed Multiplication |. Parser.symbol "*"
        ]


valueParser : Parser Expression
valueParser =
    Parser.succeed Value
        |= Parser.int


levelParser : Expression -> Parser Expression
levelParser expression1 =
    let
        merge ( op, expression2 ) =
            lift expression1 <|
                case expression2 of
                    Value _ ->
                        [ ( op, expression2 ) ]

                    Level e list ->
                        [ ( op, e ) ] ++ list
    in
    Parser.succeed Tuple.pair
        |= operatorParser
        |. Parser.spaces
        |= expressionParser
        |> Parser.map merge
        |> Parser.andThen
            (\level ->
                Parser.oneOf
                    [ levelParser level
                    , Parser.succeed level
                    ]
            )
