module Day02 exposing (..)

import List.Extra
import Maybe.Extra


solve : String -> Maybe ( Int, Int )
solve input =
    String.lines input
        |> List.map validate
        |> Maybe.Extra.combine
        |> Maybe.map count


validate : String -> Maybe ( Bool, Bool )
validate line =
    case String.split ": " line of
        [ policy, password ] ->
            case String.split " " policy of
                [ range, letter ] ->
                    case String.split "-" range of
                        [ low, high ] ->
                            Maybe.map4 check
                                (String.toInt low)
                                (String.toInt high)
                                (List.head (String.toList letter))
                                (Just password)
                                |> Maybe.Extra.join

                        _ ->
                            Nothing

                _ ->
                    Nothing

        _ ->
            Nothing


check : Int -> Int -> Char -> String -> Maybe ( Bool, Bool )
check low high letter password =
    let
        characters =
            String.toList password

        times =
            characters
                |> List.filter ((==) letter)
                |> List.length

        part1 =
            times >= low && times <= high

        part2 =
            Maybe.map2
                (\char1 char2 -> (char1 == letter) /= (char2 == letter))
                (List.Extra.getAt (low - 1) characters)
                (List.Extra.getAt (high - 1) characters)
    in
    Maybe.map2 Tuple.pair (Just part1) part2


count : List ( Bool, Bool ) -> ( Int, Int )
count list =
    List.foldl
        (\( a, b ) ( p1, p2 ) ->
            ( incrementIf a p1, incrementIf b p2 )
        )
        ( 0, 0 )
        list


incrementIf : Bool -> Int -> Int
incrementIf bool int =
    if bool then
        int + 1

    else
        int
