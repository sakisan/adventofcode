module Style exposing (..)

import Element exposing (Element)
import Element.Font as Font



-- STYLE


white =
    Element.rgb 1 1 1


black =
    Element.rgb 0 0 0


grey =
    Element.rgb255 238 238 238


blue =
    Element.rgb255 0x2B 0x4E 0x72


red =
    Element.rgb 1 0.25 0.25


yellow =
    Element.rgb255 0xFB 0xBF 0x24


backgroundColor =
    Element.rgb255 0x1E 0x3A 0x8A


fontColor =
    Element.rgb255 0xCC 0xCC 0xCC


buttonColor =
    Element.rgb255 0x06 0x5F 0x46
    -- Element.rgb255 0x92 0x40 0x0E


buttonColorActive =
    buttonColor
    -- Element.rgb255 0x04 0x78 0x57
    -- Element.rgb255 0xD9 0x77 0x06
    -- Element.rgb255 0xFB 0xBF 0x24


linkColor =
    blue


fillWidth =
    Element.width Element.fill


justBold text =
    Element.el [ Font.bold ] (Element.text text)
