module Day12 exposing (..)

import Maybe.Extra


solve : String -> Maybe ( Int, Int )
solve input =
    String.lines input
        |> List.map toInstruction
        |> Maybe.Extra.combine
        |> Maybe.map solveBoth


type Instruction
    = Forward Int
    | Rotate Turn
    | Translate Vector


type Turn
    = Straight
    | Left
    | Right
    | Back


toInstruction : String -> Maybe Instruction
toInstruction string =
    case String.uncons string of
        Nothing ->
            Nothing

        Just ( x, xs ) ->
            Maybe.andThen (construct x) (String.toInt xs)


construct : Char -> Int -> Maybe Instruction
construct x i =
    case x of
        'N' ->
            Just <| Translate (Vector (X 0) (Y i))

        'S' ->
            Just <| Translate (Vector (X 0) (Y -i))

        'W' ->
            Just <| Translate (Vector (X -i) (Y 0))

        'E' ->
            Just <| Translate (Vector (X i) (Y 0))

        'L' ->
            Just <| Rotate (repeat (i // 90) turnLeft Straight)

        'R' ->
            Just <| Rotate (repeat (i // 90) turnRight Straight)

        'F' ->
            Just <| Forward i

        _ ->
            Nothing


solveBoth : List Instruction -> ( Int, Int )
solveBoth instructions =
    let
        init1 =
            ( origin, east )

        f1 instruction ( position, direction ) =
            case instruction of
                Translate v ->
                    ( add v position, direction )

                Rotate turn ->
                    ( position, rotate turn direction )

                Forward t ->
                    ( add position (times t direction), direction )

        init2 =
            ( origin
            , origin
                |> add north
                |> add (times 10 east)
            )

        f2 instruction ( position, waypoint ) =
            case instruction of
                Translate v ->
                    ( position, add v waypoint )

                Rotate turn ->
                    ( position, rotate turn waypoint )

                Forward t ->
                    ( add position (times t waypoint), waypoint )

        ( ( ship1, _ ), ( ship2, _ ) ) =
            List.foldl (\instruction ( p1, p2 ) -> ( f1 instruction p1, f2 instruction p2 ))
                ( init1, init2 )
                instructions
    in
    ( manhattan ship1, manhattan ship2 )


origin : Vector
origin =
    Vector (X 0) (Y 0)


north : Vector
north =
    Vector (X 0) (Y 1)


south : Vector
south =
    Vector (X 0) (Y -1)


west : Vector
west =
    Vector (X -1) (Y 0)


east : Vector
east =
    Vector (X 1) (Y 0)


rotate : Turn -> Vector -> Vector
rotate turn ((Vector (X x) (Y y)) as vector) =
    case turn of
        Straight ->
            vector

        Left ->
            Vector (X -y) (Y x)

        Right ->
            Vector (X y) (Y -x)

        Back ->
            Vector (X -x) (Y -y)


turnLeft : Turn -> Turn
turnLeft turn =
    case turn of
        Straight ->
            Left

        Left ->
            Back

        Back ->
            Right

        Right ->
            Straight


turnRight : Turn -> Turn
turnRight =
    repeat 3 turnLeft


repeat : Int -> (a -> a) -> a -> a
repeat t f x =
    if t == 0 then
        x

    else
        repeat (t - 1) f (f x)



-- CUSTOM VECTOR TYPE


type Vector
    = Vector X Y


type X
    = X Int


type Y
    = Y Int


times : Int -> Vector -> Vector
times t (Vector (X x) (Y y)) =
    Vector (X (t * x)) (Y (t * y))


add : Vector -> Vector -> Vector
add (Vector (X ax) (Y ay)) (Vector (X bx) (Y by)) =
    Vector (X (ax + bx)) (Y (ay + by))


{-| Subtract a vector from another.
written with the pipeline order in mind:

    b |> subtract a --> b - a

-}
subtract : Vector -> Vector -> Vector
subtract (Vector (X ax) (Y ay)) (Vector (X bx) (Y by)) =
    Vector (X (bx - ax)) (Y (by - ay))


manhattan : Vector -> Int
manhattan (Vector (X x) (Y y)) =
    abs x + abs y
