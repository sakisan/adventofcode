module Day21 exposing (..)

import Dict exposing (Dict)
import Maybe.Extra
import Parser exposing ((|.), (|=), Parser, Trailing(..))
import Set exposing (Set)


solve : String -> Maybe ( Int, String )
solve text =
    parse text
        |> Maybe.map
            (\input ->
                let
                    allIngredients =
                        List.concatMap Tuple.first input

                    possibilities =
                        eliminate (toPossibilities input)

                    unsafeIngredients =
                        List.concat (Dict.values possibilities)

                    safeIngredients =
                        allIngredients
                            |> without unsafeIngredients

                    allergenFor ingredient =
                        Dict.toList possibilities
                            |> List.filter (\( a, i ) -> i == [ ingredient ])
                            |> List.map (\( a, i ) -> a)
                            |> List.head
                            |> Maybe.withDefault ""
                in
                ( List.length
                    (List.filter
                        (\ingredient ->
                            List.member ingredient safeIngredients
                        )
                        allIngredients
                    )
                , String.join "," (List.sortBy allergenFor unsafeIngredients)
                )
            )


type alias Input =
    List ( List String, List String )


type alias Possibilities =
    Dict String (List String)


toPossibilities : Input -> Possibilities
toPossibilities input =
    let
        insertLine ( ingredients, allergens ) dict =
            List.foldr (insertAllergen ingredients) dict allergens

        insertAllergen ingredients allergen dict =
            Dict.update allergen (update ingredients) dict

        update new present =
            case present of
                Nothing ->
                    Just new

                Just old ->
                    Just (intersect old new)
    in
    List.foldr insertLine Dict.empty input


eliminate : Possibilities -> Possibilities
eliminate possibilities =
    let
        allergens =
            Dict.keys possibilities

        after1 =
            List.foldl eliminateOne possibilities allergens

        after2 =
            List.foldl eliminateOne after1 allergens
    in
    if after1 == after2 then
        after1

    else
        eliminate after2


eliminateOne : String -> Possibilities -> Possibilities
eliminateOne allergen possibilities =
    let
        ingredients =
            case Dict.get allergen possibilities of
                Nothing ->
                    []

                Just list ->
                    list

        updateOthers =
            case ingredients of
                [ a ] ->
                    Dict.map (\_ otherIngredients -> otherIngredients |> without [ a ])

                _ ->
                    identity

        updateThis =
            Dict.insert allergen ingredients
    in
    possibilities
        |> updateOthers
        |> updateThis


intersect : List a -> List a -> List a
intersect xs ys =
    List.filter (\x -> List.member x ys) xs


without : List a -> List a -> List a
without toRemove list =
    List.filter (\elem -> not (List.member elem toRemove)) list


parse : String -> Maybe Input
parse input =
    List.map parseLine (String.lines input)
        |> Maybe.Extra.combine


parseLine : String -> Maybe ( List String, List String )
parseLine line =
    Parser.run lineParser line
        |> Result.toMaybe


lineParser : Parser ( List String, List String )
lineParser =
    Parser.succeed Tuple.pair
        |= Parser.sequence
            { start = ""
            , separator = " "
            , end = ""
            , spaces = Parser.succeed ()
            , item = alphaParser
            , trailing = Mandatory
            }
        |. Parser.spaces
        |. Parser.symbol "("
        |. Parser.keyword "contains"
        |. Parser.spaces
        |= Parser.sequence
            { start = ""
            , separator = ", "
            , end = ""
            , spaces = Parser.succeed ()
            , item = alphaParser
            , trailing = Forbidden
            }
        |. Parser.symbol ")"
        |. Parser.end


alphaParser : Parser String
alphaParser =
    Parser.getChompedString (Parser.chompWhile Char.isAlpha)


example =
    """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)"""
