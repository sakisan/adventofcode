module Day11 exposing (..)

import Array exposing (Array)
import List.Extra
import Maybe.Extra
import Set


solve : String -> Maybe ( Int, Int )
solve input =
    Maybe.andThen
        (\grid ->
            Maybe.map2 Tuple.pair
                (part1 grid)
                (part2 grid)
        )
        (parse input ( 0, 0 ))


type alias Grid =
    Array (Array Seat)


type Seat
    = Empty
    | Occupied
    | Floor


parse : String -> ( Int, Int ) -> Maybe Grid
parse string ( x, y ) =
    String.lines string
        |> List.map
            (String.toList >> List.map toSeat >> Maybe.Extra.combine >> Maybe.map Array.fromList)
        |> Maybe.Extra.combine
        |> Maybe.map Array.fromList


toSeat : Char -> Maybe Seat
toSeat char =
    case char of
        '.' ->
            Just Floor

        '#' ->
            Just Occupied

        'L' ->
            Just Empty

        _ ->
            Nothing


part1 : Grid -> Maybe Int
part1 grid =
    stabilize (rule1 0 directions8) (gridKeys grid) grid
        |> countAllOccupiedSeats
        |> Just


rule1 : Int -> List ( Int, Int ) -> Seat -> ( Int, Int ) -> Grid -> Seat
rule1 count directions previous position grid =
    case directions of
        [] ->
            if count == 0 then
                Occupied

            else
                previous

        direction :: more ->
            let
                newCount =
                    count + eval (add direction position) grid
            in
            if count == 1 && previous == Empty then
                Empty

            else if newCount == 4 && previous == Occupied then
                Empty

            else
                rule1 newCount more previous position grid


part2 : Grid -> Maybe Int
part2 grid =
    stabilize (rule2 0 directions8) (gridKeys grid) grid
        |> countAllOccupiedSeats
        |> Just


rule2 : Int -> List ( Int, Int ) -> Seat -> ( Int, Int ) -> Grid -> Seat
rule2 count directions previous position grid =
    case directions of
        [] ->
            if count == 0 then
                Occupied

            else
                previous

        direction :: more ->
            let
                newCount =
                    count + lookupInDirection grid position direction
            in
            if count == 1 && previous == Empty then
                Empty

            else if newCount == 5 && previous == Occupied then
                Empty

            else
                rule2 newCount more previous position grid


lookupInDirection : Grid -> ( Int, Int ) -> ( Int, Int ) -> Int
lookupInDirection grid pos direction =
    let
        nextPos =
            add pos direction
    in
    case get nextPos grid of
        Just Occupied ->
            1

        Just Empty ->
            0

        Nothing ->
            0

        _ ->
            lookupInDirection grid nextPos direction


stabilize :
    (Seat -> ( Int, Int ) -> Grid -> Seat)
    -> List ( Int, Int )
    -> Grid
    -> Grid
stabilize update keys grid =
    let
        -- _ = Debug.log (show grid) ""
        -- _ = Debug.log (show newGrid) ""
        ( newGrid, updatedKeys ) =
            List.foldl helper ( grid, [] ) keys

        helper key ( accGrid, accKeys ) =
            let
                previous =
                    get key grid
                        |> Maybe.withDefault Empty
            in
            if previous /= Floor then
                let
                    updated =
                        update previous key grid
                in
                if previous /= updated then
                    ( insert key updated accGrid, key :: accKeys )

                else
                    ( accGrid, accKeys )

            else
                ( accGrid, accKeys )
    in
    if List.isEmpty updatedKeys then
        newGrid

    else
        stabilize update updatedKeys newGrid


countAllOccupiedSeats : Grid -> Int
countAllOccupiedSeats grid =
    grid
        |> values
        |> List.filter (\seat -> seat == Occupied)
        |> List.length


eval : ( Int, Int ) -> Grid -> Int
eval pos grid =
    if get pos grid == Just Occupied then
        1

    else
        0


left =
    ( 0, -1 )


right =
    ( 0, 1 )


up =
    ( -1, 0 )


down =
    ( 1, 0 )


directions8 =
    [ up
    , down
    , right
    , left
    , add up left
    , add up right
    , add down left
    , add down right
    ]


add : ( Int, Int ) -> ( Int, Int ) -> ( Int, Int )
add ( a, b ) ( i, j ) =
    ( a + i, b + j )


show : Grid -> String
show grid =
    let
        cs =
            List.map (\y -> List.map (\x -> ( x, y )) (List.range 0 9)) (List.range 0 9)

        s seat =
            case seat of
                Occupied ->
                    "#"

                Empty ->
                    "L"

                Floor ->
                    "."
    in
    List.map (String.join "" << List.map (\c -> get c grid |> Maybe.map s |> Maybe.withDefault ".")) cs
        |> String.join "\n"
        |> (++) "\n"


insert : ( Int, Int ) -> Seat -> Grid -> Grid
insert ( x, y ) seat grid =
    let
        row =
            Maybe.withDefault Array.empty (Array.get y grid)
    in
    Array.set y (Array.set x seat row) grid


get : ( Int, Int ) -> Grid -> Maybe Seat
get ( x, y ) grid =
    Array.get y grid
        |> Maybe.andThen (\row -> Array.get x row)


gridKeys : Grid -> List ( Int, Int )
gridKeys grid =
    let
        xBound =
            (Array.get 0 grid |> Maybe.map Array.length |> Maybe.withDefault 0) - 1

        yBound =
            Array.length grid - 1
    in
    List.concatMap (\y -> List.map (\x -> ( x, y )) (List.range 0 xBound)) (List.range 0 yBound)


values : Grid -> List Seat
values grid =
    grid
        |> Array.map Array.toList
        |> Array.toList
        |> List.concat


input1 =
    """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"""
