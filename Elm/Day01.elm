module Day01 exposing (..)


solve : String -> Maybe ( Int, Int )
solve input =
    let
        numbers =
            input
                |> String.lines
                |> List.filterMap String.toInt

        part1 =
            combineWith Tuple.pair numbers numbers
                |> List.filter (\( a, b ) -> a + b == 2020)
                |> List.head
                |> Maybe.map (\( a, b ) -> a * b)

        part2 =
            combineWith3 (\a b c -> ( a, b, c )) numbers numbers numbers
                |> List.filter (\( a, b, c ) -> a + b + c == 2020)
                |> List.head
                |> Maybe.map (\( a, b, c ) -> a * b * c)
    in
    Maybe.map2 Tuple.pair part1 part2


combineWith : (a -> b -> c) -> List a -> List b -> List c
combineWith function listA listB =
    List.concatMap
        (\a ->
            List.map (\b -> function a b) listB
        )
        listA


combineWith3 : (a -> b -> c -> d) -> List a -> List b -> List c -> List d
combineWith3 function listA listB listC =
    List.concatMap
        (\a ->
            List.concatMap
                (\b ->
                    List.map (\c -> function a b c) listC
                )
                listB
        )
        listA
