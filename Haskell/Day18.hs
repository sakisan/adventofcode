import Data.Char
import Debug.Trace

main :: IO ()
main = do
    input0 <- parse <$> readFile "Day18.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- test ---" >> solve input2
    putStrLn "--- input ---" >> solve input0

solve input = do
    -- putStr "part 1: "
    -- print $ part1 input
    putStr "part 2: "
    print $ part2 input

    -- wrong answers (part 1)
    -- 4528554924428
    -- 13575733599054
    -- 4738008326678 too low
    --
    -- wrong answers (part 2)
    -- 449182392080018 too loo



----                                ----
--                                    --
-- This is terrible, don't read it xD --
--                                    --
----                                ----


type Input = [String]

parse :: String -> Input
parse = lines

part1 :: Input -> Int
part1 input = sum $ traceShowId $ map (calc [] []) input

data Op
    = Add
    | Mul
    deriving (Show)

calc :: [Int] -> [Op] -> String -> Int
calc [n] _ [] = n
calc ns ops (x:xs)
    | x == '(' = calc (0 : ns) ops xs
    | x == ')' = t$
        case ns of
            (n:0:ns') -> 
                let (ns'', ops') = applyAll (Add:ops) ns
                in calc ns'' ops' xs
            _ -> 
                let (ns'', ops') = applyAll ops ns
                in calc ns'' ops' xs
    | x == '+' = t $ calc ns (Add : ops) xs
    | x == '*' = t $ calc ns (Mul : ops) xs
    | x == ' ' = t $ calc ns ops xs
    | isDigit x =
        t $
        let n = (read [x] :: Int)
        in case ns of
               (0:ns') -> calc (n : ns') ops xs
               _ ->
                   let (ns', ops') = applyAll ops (n : ns)
                   in calc ns' ops' xs
  where
    -- t = traceShow (ns, ops, x : xs)
    t = id 
calc ns ops xs = traceShow (ns, ops, xs) $ undefined

applyAll :: [Op] -> [Int] -> ([Int], [Op])
applyAll [] ns = (ns, [])
applyAll ops [] = ([], ops)
applyAll ops [n] = ([n], ops)
applyAll (o:os) (a:b:ns) = (((apply o a b) : ns), os)

apply :: Op -> Int -> Int -> Int
apply Add = (+)
apply Mul = (*)



part2 :: Input -> Int
part2 input = sum $ traceShowId $ map (calc2 [] []) input

calc2 :: [[Int]] -> [Op] -> String -> Int
calc2 ns ops [] = let ([[n]],[]) = resolveParens ops ns in n
calc2 ns ops (x:xs)
    | x == '(' = t$ calc2 ([] : ns) ops xs
    | x == ')' = t$
        let (ns'', ops') = resolveParens ops ns
        in case (ns'',ops') of
            (((a:b:n1):ns'''), Add:os) -> calc2 ((a+b:n1):ns''') os xs
            _ -> calc2 ns'' ops' xs
    | x == '+' = t $ calc2 ns (Add : ops) xs
    | x == '*' = t $ calc2 ns (Mul : ops) xs
    | x == ' ' = t $ calc2 ns ops xs
    | isDigit x =
        t $
        let n = (read [x] :: Int)
        in case ns of
               [] -> calc2 [[n]] ops xs
               ([]:ns') -> calc2 ([n] : ns') ops xs
               ((n':n1):ns') ->
                    case ops of
                        (Add:os) -> calc2 (((n + n'):n1):ns') os xs
                        _ -> calc2 ((n:n':n1):ns') ops xs
  where
    -- t = traceShow (ns, ops, x : xs)
    t = id 
calc2 ns ops xs = traceShow (ns, ops, xs) $ undefined

resolveParens :: [Op] -> [[Int]] -> ([[Int]], [Op])
resolveParens (Mul:ops) ((a:b:ns):nss) = resolveParens ops ((a*b:ns):nss)
resolveParens (Add:ops) ((a:b:ns):nss) = resolveParens ops ((a+b:ns):nss)
resolveParens ops ([a]:(n:nss)) = ((a:n):nss, ops)
resolveParens ops [[a]] = ([[a]], ops)

input2 =
    [ "((1 + 1) + (2 + 2) * (5 + 5))"
    -- 2 + 4 * 10 = 60
    ]

input1 =
    [ "1 + 2 * 3 + 4 * 5 + 6"
    , "1 + (2 * 3) + (4 * (5 + 6))"
    , "2 * 3 + (4 * 5)"
    , "5 + (8 * 3 + 9 + 3 * 4 * 3)"
    , "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"
    , "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"
    ]
