{-# LANGUAGE BangPatterns #-}

import Control.Applicative
import Control.Monad
import Data.Array
import Data.Char
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace
import Text.Read
import Text.Printf
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day17.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1: "
    print $ part1 parsed
    -- putStr "part 2: "
    -- print $ part2 parsed

type Input = [String]

parse :: [String] -> Input
parse = id

part1 :: Input -> Int
part1 input = length $ concat input

input1 = [


    ]
