import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day24.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1: "
    print $ part1 parsed
    putStr "part 2: "
    print $ part2 parsed

data Direction
    = E
    | SE
    | SW
    | W
    | NW
    | NE
    deriving (Eq, Show)

type Input = [[Direction]]

parse :: [String] -> Input
parse = map parseDirections

parseDirections :: String -> [Direction]
parseDirections = f
  where
    f [] = []
    f ('s':'e':xs) = SE : f xs
    f ('s':'w':xs) = SW : f xs
    f ('n':'e':xs) = NE : f xs
    f ('n':'w':xs) = NW : f xs
    f ('e':xs) = E : f xs
    f ('w':xs) = W : f xs

part1 :: Input -> Int
part1 input =
    length . filter (odd . length) . group . sort $ map endCoordinate input

endCoordinate :: [Direction] -> (Int, Int)
endCoordinate = foldr addCoordinate (0, 0)

addCoordinate :: Direction -> (Int, Int) -> (Int, Int)
addCoordinate direction (x, y) = f direction
  where
    f E = (x - 1, y)
    f W = (x + 1, y)
    f SE = (x - 1 + dx, y + 1)
    f NE = (x - 1 + dx, y - 1)
    f SW = (x + dx, y + 1)
    f NW = (x + dx, y - 1)
    dx
        | even y = 1
        | otherwise = 0

type Floor = Set (Int, Int)

part2 :: Input -> Int
part2 input = Set.size (days !! 100)
  where
    floor =
        Set.fromList . map head . filter (odd . length) . group . sort $
        map endCoordinate input
    days = iterate tick floor

tick :: Floor -> Floor
tick floor =
    traceShow (Set.size floor) $
    Set.fromList [c | (c, count) <- Map.assocs counts, isNewGeneration c count]
  where
    isNewGeneration c count
        | isBlack c = not (count == 0 || count > 2)
        | otherwise = count == 2
    tilesToCount = foldMap (\c -> Set.fromList (c : getNeighbors c)) floor
    counts =
        Map.fromListWith (+) $
        [(x, 1) | c <- Set.elems tilesToCount, isBlack c, x <- getNeighbors c]
    isBlack c = Set.member c floor

getNeighbors c = addCoordinate <$> [E, W, SE, SW, NE, NW] <*> [c]

input1 =
    [ "sesenwnenenewseeswwswswwnenewsewsw"
    , "neeenesenwnwwswnenewnwwsewnenwseswesw"
    , "seswneswswsenwwnwse"
    , "nwnwneseeswswnenewneswwnewseswneseene"
    , "swweswneswnenwsewnwneneseenw"
    , "eesenwseswswnenwswnwnwsewwnwsene"
    , "sewnenenenesenwsewnenwwwse"
    , "wenwwweseeeweswwwnwwe"
    , "wsweesenenewnwwnwsenewsenwwsesesenwne"
    , "neeswseenwwswnwswswnw"
    , "nenwswwsewswnenenewsenwsenwnesesenew"
    , "enewnwewneswsewnwswenweswnenwsenwsw"
    , "sweneswneswneneenwnewenewwneswswnese"
    , "swwesenesewenwneswnwwneseswwne"
    , "enesenwswwswneneswsenwnewswseenwsese"
    , "wnwnesenesenenwwnenwsewesewsesesew"
    , "nenewswnwewswnenesenwnesewesw"
    , "eneswnwswnwsenenwnwnwwseeswneewsenese"
    , "neswnwewnwnwseenwseesewsenwsweewe"
    , "wseweeenwnesenwwwswnew"
    ]
