import Data.List
import Data.List.Split
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day19.txt"
    putStrLn "--- example 1 ---" >> solve input1
    putStrLn "--- example 2 ---" >> solve input2
    putStrLn "--- example 3 ---" >> solve input3
    putStrLn "--- example 4 ---" >> solve input4
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed@(rules, messages) = parse input
    putStr "part 1: "
    print $ part1 parsed
    putStr "part 2: "
    print $
        part1
            ( Map.insert 8 (snd (parseRule "8: 42 | 42 8")) $
              Map.insert 11 (snd (parseRule "11: 42 31 | 42 11 31")) $ rules
            , messages)

type Input = (Map Int Rule, [String])

data Rule
    = Literal String
    | Sequence [Int]
    | Or [Int]
         [Int]
    deriving (Show)

type Rules = Map Int Rule

parse :: [String] -> Input
parse input = (parsedRules, messages)
  where
    [rules, messages] = splitOn [""] input
    parsedRules = Map.fromList $ map parseRule rules

parseRule :: String -> (Int, Rule)
parseRule rule
    | body == "\"a\"" = (n, Literal "a")
    | body == "\"b\"" = (n, Literal "b")
    | '|' `elem` body = (n, Or (readInts p1) (readInts p2))
    | otherwise = (n, Sequence (readInts body))
  where
    n = read head :: Int
    [head, body] = splitOn ": " rule
    [p1, p2] = splitOn "|" body

readInts :: String -> [Int]
readInts = map read . words

part1 :: Input -> Int
part1 (rules, messages) = length $ filter (valid rules) messages

valid rules message = "" `elem` (match rules 0 message)

match :: Rules -> Int -> String -> [String]
match rules n "" = [""]
match rules n string =
    case rules Map.! n of
        Literal a ->
            if a `isPrefixOf` string
                then [tail string]
                else []
        Sequence ns -> matchSequence rules ns string
        Or ns1 ns2 ->
            nub $
            concat
                [matchSequence rules ns1 string, matchSequence rules ns2 string]

matchSequence :: Rules -> [Int] -> String -> [String]
matchSequence rules [] "" = [""]
matchSequence rules [] string = [string]
matchSequence rules (n:ns) string =
    case match rules n string of
        [] -> []
        matches ->
            if "" `elem` matches
                then case ns of
                         [] -> [""]
                         (n':ns') -> []
                else nub $ concatMap (matchSequence rules ns) matches

input1 = ["0: 1 2", "1: \"a\"", "2: 1 3 | 3 1", "3: \"b\"", "", "aab", "aba"]

input2 =
    [ "0: 4 1 5"
    , "1: 2 3 | 3 2"
    , "2: 4 4 | 5 5"
    , "3: 4 5 | 5 4"
    , "4: \"a\""
    , "5: \"b\""
    , ""
    , "aaaabb"
    , "aaabab"
    , "abbabb"
    , "abbbab"
    , "aabaab"
    , "aabbbb"
    , "abaaab"
    , "ababbb"
    ]

input3 =
    [ "0: 4 1 5"
    , "1: 2 3 | 3 2"
    , "2: 4 4 | 5 5"
    , "3: 4 5 | 5 4"
    , "4: \"a\""
    , "5: \"b\""
    , ""
    , "ababbb"
    , "bababa"
    , "abbbab"
    , "aaabbb"
    , "aaaabbb"
    ]

input4 =
    [ "0: 8 11"
    , "10: 23 14 | 28 1"
    , "11: 42 31"
    , "12: 24 14 | 19 1"
    , "13: 14 3 | 1 12"
    , "14: \"b\""
    , "15: 1 | 14"
    , "16: 15 1 | 14 14"
    , "17: 14 2 | 1 7"
    , "18: 15 15"
    , "19: 14 1 | 14 14"
    , "1: \"a\""
    , "20: 14 14 | 1 15"
    , "21: 14 1 | 1 14"
    , "22: 14 14"
    , "23: 25 1 | 22 14"
    , "24: 14 1"
    , "25: 1 1 | 1 14"
    , "26: 14 22 | 1 20"
    , "27: 1 6 | 14 18"
    , "28: 16 1"
    , "2: 1 24 | 14 4"
    , "31: 14 17 | 1 13"
    , "3: 5 14 | 16 1"
    , "42: 9 14 | 10 1"
    , "4: 1 1"
    , "5: 1 14 | 15 1"
    , "6: 14 14 | 1 14"
    , "7: 14 5 | 1 21"
    , "8: 42"
    , "9: 14 27 | 1 26"
    , ""
    , "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa"
    , "bbabbbbaabaabba"
    , "babbbbaabbbbbabbbbbbaabaaabaaa"
    , "aaabbbbbbaaaabaababaabababbabaaabbababababaaa"
    , "bbbbbbbaaaabbbbaaabbabaaa"
    , "bbbababbbbaaaaaaaabbababaaababaabab"
    , "ababaaaaaabaaab"
    , "ababaaaaabbbaba"
    , "baabbaaaabbaaaababbaababb"
    , "abbbbabbbbaaaababbbbbbaaaababb"
    , "aaaaabbaabaaaaababaa"
    , "aaaabbaaaabbaaa"
    , "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa"
    , "babaaabbbaaabaababbaabababaaab"
    , "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"
    ]
