import Data.Array
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day11.txt"
    let input = input1
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve lines = do
    let numbers = parse lines
    putStr "part 1: "
    print $ part1 numbers
    putStr "part 2: "
    print $ part2 numbers

parse lines = toArray lines

toArray :: [String] -> Array (Int,Int) Char
toArray input = listArray ((0, 0), (h - 1, w - 1)) (concat input)
    where 
        h = length input
        w = length (input !! 0)

showArray :: Array (Int,Int) Char -> String
showArray a = unlines $ [] : chunksOf (w+1) (elems a)
    where (_,(h,w)) = bounds a


-- PART 1

part1 a =  countOccupied a'
    where a' = turnWhile (countOccupied a) a 
         
turnWhile n a = -- trace (showArray a' ++ "\n" ++ show n') $ 
    if n' == n then a'
    else turnWhile n' a'
    where a' = turn a
          n' = countOccupied a'

turn a = a // map t (assocs a)
    where t (x,s) = (x, s')
            where n = adjacents a x
                  s' = case s of
                        'L' -> if n == 0 then '#' else 'L'
                        '#' -> if n >= 4 then 'L' else '#'
                        y -> y

adjacents a x = length $ filter isOccupied $ map (a!) $ filter (inRange (bounds a))(neighbours x)

-- PART 2

part2 a =  countOccupied a'
    where a' = turnWhile2 (countOccupied a) a 
         
turnWhile2 n a = -- trace (showArray a' ++ "\n" ++ show n') $ 
    if n' == n then a'
    else turnWhile2 n' a'
    where a' = turn2 a
          n' = countOccupied a'

turn2 a = a // map t (assocs a)
    where t (x,s) = (x, s')
            where n = adjacents2 a x
                  s' = case s of
                        'L' -> if n == 0 then '#' else 'L'
                        '#' -> if n >= 5 then 'L' else '#'
                        y -> y


adjacents2 a x = length $ mapMaybe (lookupInDirection a x) directions


lookupInDirection grid pos direction =
    let nextPos = (add pos direction)
    in if inRange (bounds grid) nextPos
           then let x = grid ! nextPos
                in if isOccupied x then Just x
                    else if isEmpty x then Nothing
                       else lookupInDirection grid nextPos direction
           else Nothing


-- COMMON

countOccupied a = length $ filter isOccupied $ elems a

neighbours x = map (add x) directions

add :: (Int,Int) -> (Int,Int) -> (Int,Int)
add (a,b) (i,j) = (a + i, b + j)    

directions = [up,down,right,left, add up left, add up right, add down left, add down right]
left = (0,-1)
right = (0,1)
up = (-1,0)
down = (1,0)

isSeat x = isOccupied x || isEmpty x
isOccupied x = x == '#'
isEmpty x = x == 'L'

input1 = [
    "L.LL.LL.LL",
    "LLLLLLL.LL",
    "L.L.L..L..",
    "LLLL.LL.LL",
    "L.LL.LL.LL",
    "L.LLLLL.LL",
    "..L.L.....",
    "LLLLLLLLLL",
    "L.LLLLLL.L",
    "L.LLLLL.LL"
    ]
