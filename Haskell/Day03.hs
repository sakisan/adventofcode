import Control.Applicative
import Data.List
import Data.Array
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day03.txt"
    let input = input0
    print $ solve1 input
    print $ solve2 input

solve1 input = countTrees woods (1,3) (0,0) 0
    where woods = toArray input

solve2 input =
    product $
    map (\d -> countTrees woods d (0, 0) 0) $
    [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    where woods = toArray input

countTrees woods (dy,dx) (y,x) n =
    if y > yMax  then
        n
    else
        countTrees woods (dy, dx) (y + dy, (x + dx) `mod` (xMax + 1)) $
        if woods ! (y, x) == '#' then n + 1 else n
    where
        (_, (yMax, xMax)) = bounds woods

toArray :: [String] -> Array (Int,Int) Char
toArray input = listArray ((0, 0), (h - 1, w - 1)) (concat input)
    where 
        h = length input
        w = length (input !! 0)

input1 = [
    "..##.......",
    "#...#...#..",
    ".#....#..#.",
    "..#.#...#.#",
    ".#...##..#.",
    "..#.##.....",
    ".#.#.#....#",
    ".#........#",
    "#.##...#...",
    "#...##....#",
    ".#..#...#.#"
    ]
