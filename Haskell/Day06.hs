import Data.List
import Data.List.Split

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day06.txt"
    let input = input1
    print $ solve1 input
    print $ solve2 input

solve1 input = sum $ map count $ splitOn [[]] input
  where
    count = length . nub . sort . concat

solve2 input = sum $ map count $ splitOn [[]] input
  where
    count [] = 0
    count (a:as) = length $ foldr intersect a as

input1 = [

    "abc",
    "",
    "a",
    "b",
    "c",
    "",
    "ab",
    "ac",
    "",
    "a",
    "a",
    "a",
    "a",
    "",
    "b"

    ]
