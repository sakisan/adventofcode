import Control.Applicative
import Data.Array
import Data.Char
import Data.List
import Data.List.Split
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day05.txt"
    let input = input0
    print $ solve1 input
    print $ solve2 input

solve1 input = maximum $ map seatID input

-- I printed this first then saw the first number (81) and used that below
-- solve2 input = sort $ map seatID input
solve2 input = 81 + (length $ takeWhile (== True) $ zipWith (==) [81 ..] $ sort $ map seatID input)

seatID pass = 8 * row + col
  where
    (row, col) = (toBinary 'B' 'F' fb, toBinary 'R' 'L' rl)
    fb = take 7 pass
    rl = drop 7 pass

toBinary o z = foldl (\a x -> 2 * a + t x) 0
  where
    t c
        | c == o = 1
        | c == z = 0

input1 = ["FBFBBFFRLR", "BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"]
