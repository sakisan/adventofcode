import Data.Ix
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day17.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    putStr "part 1: "
    putStrLn $ show . part2 $ parse input

type Pocket = Map (Int,Int,Int,Int) Bool

parse :: [String] -> Pocket
parse input = Map.fromList (addCoordinates input)

addCoordinates :: [[Char]] -> [((Int,Int,Int,Int),Bool)]
addCoordinates = concat . zipWith (\y -> zipWith (\x c -> ((0,0,y,x),c=='#')) [0..]) [0..]

part2 :: Pocket -> Int
part2 pocket = length $ Map.elems after6
    -- "\n" ++ display pocket ++ 
    -- "\n" ++ display (tick pocket)
    -- "\n" ++ display ((tick . tick) pocket)
    -- "\n" ++ display ((tick . tick . tick) pocket)
    where after6 = (tick . tick . tick . tick . tick . tick) pocket

tick :: Pocket -> Pocket
tick pocket = 
    foldr f Map.empty (collectAllNeighbours (Map.keys pocket))
    where f k m 
            | a == True = if count == 2 || count == 3 then Map.insert k True m else m
            | a /= True = if count == 3 then Map.insert k True m else m
            where count = countActiveNeighbours k pocket
                  a = fromMaybe False (Map.lookup k pocket)

countActiveNeighbours :: (Int,Int,Int,Int) -> Pocket -> Int
countActiveNeighbours (w,z,y,x) pocket =
    length . filter id . mapMaybe (`Map.lookup` pocket) $
        collectNeighbours (w,z,y,x)

collectNeighbours :: (Int,Int,Int,Int) -> [(Int,Int,Int,Int)]
collectNeighbours (w, z,y,x) =
    map (\(dw, dz, dy, dx) -> (w+dw, z+dz, y+dy, x+dx)) $
    filter (/= (0, 0, 0, 0)) $ range ((-1, -1, -1, -1), (1, 1, 1, 1))
            
collectAllNeighbours :: [(Int,Int,Int,Int)] -> [(Int,Int,Int,Int)]
collectAllNeighbours keys = nub $ concatMap collectNeighbours keys


-- display :: Pocket -> String
-- display pocket = intercalate "\n" $ map displaySlice [z_min .. z_max]
--     where 
--         ((z_max,_,_),_) = Map.findMax pocket
--         ((z_min,_,_),_) = Map.findMin pocket
--         displaySlice z =
--             unlines . chunksOf 6 $
--             map (\(y, x) -> maybe '.' displayCube (Map.lookup (z, y, x) pocket))
--                 (range ((-1, -1),(4, 4)) )
--         displayCube True = '#'
--         displayCube False = '.'


input1 = [
    ".#.",
    "..#",
    "###"
    ]
