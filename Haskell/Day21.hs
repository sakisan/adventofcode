import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day21.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1 and 2: "
    print $ part1and2 parsed

type Input = [String]

type InputMap = Map String [[String]]

parse :: [String] -> [String]
parse = id

toMap :: [String] -> InputMap
toMap = foldr insertLine Map.empty

insertLine line map = foldr insertAllergen map allergens
  where
    insertAllergen allergen = Map.insertWith (++) allergen [ingredients]
    (pre, post) = break (== '(') line
    ingredients = words pre
    allergens =
        words $
        filter (/= ',') $ drop (length "(contains ") $ takeWhile (/= ')') post

part1and2 :: Input -> (Int, String)
part1and2 input =
    ( length . filter (`elem` free) . words $ unlines input
    , intercalate "," $ sortOn alphabeticAllergen locked)
  where
    inputmap = toMap input
    eliminated = eliminate inputmap (Map.keys inputmap)
    locked = concat . concat $ Map.elems eliminated
    free = nub $ (concat . concat $ Map.elems inputmap) `without` locked
    alphabeticAllergen i =
        fst . head . filter (\(allergen, [[ingredient]]) -> ingredient == i) $
        Map.toList eliminated

eliminate :: InputMap -> [String] -> InputMap
eliminate m allergens =
    if after1 == after2
        then after1
        else eliminate after2 allergens
  where
    after1 = foldr eliminateOne m allergens
    after2 = foldr eliminateOne after1 allergens

eliminateOne :: String -> InputMap -> InputMap
eliminateOne allergen m = updateThis . updateOthers $ m
  where
    Just lists = Map.lookup allergen m
    possibilities = foldr1 intersect lists
    updateThis = Map.insert allergen [possibilities]
    updateOthers =
        case possibilities of
            [a] -> fmap (map (`without` [a]))
            _ -> id

without :: Eq a => [a] -> [a] -> [a]
without list toRemove = filter (`notElem` toRemove) list

input1 =
    [ "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)"
    , "trh fvjkl sbzzf mxmxvkd (contains dairy)"
    , "sqjhc fvjkl (contains soy)"
    , "sqjhc mxmxvkd sbzzf (contains fish)"
    , ""
    ]
