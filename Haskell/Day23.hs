{-# LANGUAGE BangPatterns #-}

import Control.Monad.ST
import Data.Array.ST


import qualified Data.Vector.Unboxed.Mutable as MV
import Control.Applicative
import Control.Monad
import Data.Array
import Data.Char
import Data.Foldable
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Semigroup
import Debug.Trace
import Text.Printf
import Text.Read

import Data.Sequence (Seq(..),(|>),(<|),(><))
import qualified Data.Sequence as Seq

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day23.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1: "
    print $ part1 parsed
    putStr "part 2: "
    print $ part2 parsed

type Input = [Int]

parse :: [String] -> Input
parse = map (read . pure) . head

part1 :: Input -> Int
part1 input = toInt . set1 $ rounds !! 100
  where
    rounds = iterate move input

toInt :: [Int] -> Int
toInt = foldl (\acc n -> 10 * acc + n) 0

pushBack :: [Int] -> [Int]
pushBack (x:xs) = xs ++ [x]

set1 :: [Int] -> [Int]
set1 (1:xs) = xs
set1 xs = set1 $ pushBack xs

move :: [Int] -> [Int]
move cups@(current:clockwise) = newCups
  where
    (pickedUp, afterPickup) = splitAt 3 clockwise
    target = nextTarget pickedUp (length cups) (current - 1)
    (head, (_:tail)) = break (== target) afterPickup
    newCups = concat $ [head, [target], pickedUp, tail, [current]]

nextTarget :: [Int] -> Int -> Int -> Int
nextTarget pickedUp highest 0 = nextTarget pickedUp highest highest
nextTarget pickedUp highest current
    | current `notElem` pickedUp = current
    | otherwise = nextTarget pickedUp highest (current - 1)

{- first time using mutable arrays, yay! -}
part2 input = runST $ do 
    let size = one_million
    let rounds = ten_million
    v <- newArray (0,size) (input!!0) :: ST s (STArray s Int Int)
    forM_ ((zip <*> tail) input) $ \(i,cup) -> do
        writeArray v i cup
    writeArray v (last input) 10
    forM_ [10..size] $ \i -> do
        writeArray v i (i+1)
    writeArray v size (input!!0)
    replicateM_ rounds $ do
        current <- readArray v 0
        a <- readArray v current
        b <- readArray v a
        c <- readArray v b
        d <- readArray v c
        let target = nextTarget [a,b,c] size (current - 1)
        afterTarget <- readArray v target
        writeArray v current d
        writeArray v target a
        writeArray v c afterTarget
        writeArray v 0 d
    a1 <- readArray v 1
    a2 <- readArray v a1
    return (a1,a2,a1*a2)


one_million = 1000000

ten_million = 10000000


{- valiant effort, but still too slow
moveSeq :: Seq Int -> Seq Int
moveSeq cups@(current :<| a :<| b :<| c :<| afterPickup) = 
    newCups
  where
    pickedUp = Seq.fromList [a,b,c]
    target = nextTarget [a,b,c] (Seq.length cups) (current - 1)
    (head, tail) =
        findBreak target (Seq.empty, afterPickup) (Seq.empty, afterPickup)
    newCups = ((head |> target) >< pickedUp >< tail) |> current

findBreak :: Int -> (Seq Int, Seq Int) -> (Seq Int, Seq Int) -> (Seq Int, Seq Int)
findBreak target (cameBefore, comesBefore :|> left) (cameAfter, right :<| comesAfter)
    | left == target = (comesBefore, cameBefore)
    | right == target = (cameAfter, comesAfter)
    | otherwise = 
        findBreak
            target
            (left <| cameBefore, comesBefore)
            (cameAfter |> right, comesAfter)
-}

-- (3) 8  9  1  2  5  4  6  7 
--  3 (2) 8  9  1  5  4  6  7
--  3  2 (5) 4  6  7  8  9  1
--  7  2  5 (8) 9  1  3  4  6
--  3  2  5  8 (4) 6  7  9  1
--  9  2  5  8  4 (1) 3  6  7
--  7  2  5  8  4  1 (9) 3  6 
--  8  3  6  7  4  1  9 (2) 5
--  7  4  1  5  8  3  9  2 (6)
--
-- (3) 8  9  1 (2) 5  4  6  7 
--  3 (2) 8  9  1 (5) 4  6  7  ---   3 (4 6 7) 2 5 (8 9 1)
--  3  2 (5) 4  6  7 (8) 9  1
--  7  2  5 (8) 9  1  3 (4) 6
--  3  2  5  8 (4) 6  7  9 (1)
--
-- (3) 8  9  1  2  5  4  6  7  
--  3 (2) 8  9  1  5  4  6  7
--  3  2 (5) 4  6  7  8  9  1
--  7  2  5 (8) 9  1  3  4  6
--  3  2  5  8 (4) 6  7  9  1
--  9  2  5  8  4 (1) 3  6  7
--  7  2  5  8  4  1 (9) 3  6 
--  8  3  6  7  4  1  9 (2) 5
--  7  4  1  5  8  3  9  2 (6)

--  ... (c) a b c (n)  ... c-1 ...
--       i        i+4      i+d 
--
--  i+1 to i+4 -> +d
--  i+5 to i+d -> -3
--  remaining  -> ==


{- way too slow 
moveArray :: (Int, Array Int Int) -> (Int, Array Int Int)
moveArray (cursor, cups)
    -- traceShow (current, pickedUp , target) $
 = (wrap (cursor + 1), newCups)
  where
    (_, size) = bounds cups
    wrap i
        | i <= 0 = wrap (size + 1 + i)
        | otherwise = i `mod` (size + 1)
    current = cups ! cursor
    newCups = cups // (map moveIndex [(x, cups ! x) | x <- ixToUpdate])
    ixToUpdate =
        takeWhile (`isBetween` (start, end)) [wrap x | x <- [cursor + 1 ..]]
    start = wrap (cursor + 1)
    end = wrap (cursor + offset)
    moveIndex (i, e)
        | i `isBetween` (start, wrap (cursor + 3)) = (wrap (i + offset - 3), e)
        | i `isBetween` (wrap (cursor + 4), end) = (wrap (i - 3), e)
    pickedUp = [cups ! wrap (cursor + x) | x <- [1, 2, 3]]
    target = nextTarget pickedUp (size + 1) (current - 1)
    Just targetIndex = elemIndex target $ toList cups
    offset
        | cursor < targetIndex = targetIndex - cursor
        | otherwise = size + 1 - (cursor - targetIndex)
    isBetween i (a, b)
        | a <= b = a <= i && i <= b
        | otherwise = i <= b || a <= i
-}

{- way too slow 
moveLinkArray :: (Int, Array Int Int) -> (Int, Array Int Int)
moveLinkArray (current, array) = (d, newArray)
  where
    (_, size) = bounds array
    a = array ! current
    b = array ! a
    c = array ! b
    d = array ! c
    target = nextTarget [a, b, c] (size) (current - 1)
    afterTarget = array ! target
    newArray = array // [(current, d), (target, a), (c, afterTarget)]
-}


{- way too slow 
move2Arrays ::
       (Int, Int, Array Int Int, Array Int Int)
    -> (Int, Int, Array Int Int, Array Int Int)
move2Arrays (!cursor, !size, !values, !indexes) =
    (wrap (cursor + 1), size, newValues, newIndexes)
  where
    wrap i
        | i <= 0 = (size + i) `mod` size
        | otherwise = i `mod` size
    current = values ! cursor
    pickedUpIndexes = [wrap (cursor + x) | x <- [1, 2, 3]]
    pickedUp = [values ! x | x <- pickedUpIndexes]
    target = nextTarget pickedUp (size) (current - 1)
    targetIndex = indexes ! target
    offset
        | cursor < targetIndex = targetIndex - cursor
        | otherwise = size - (cursor - targetIndex)
    indexesToUpdate1 = take offset [cursor + 1 .. size - 1]
    l1 = length indexesToUpdate1
    indexesToUpdate2 = take (offset - l1) [0 ..]
    indexesToUpdate = indexesToUpdate2 ++ (drop 3 indexesToUpdate1)
    (updateV, updateI) =
        let (toWrap, others) =
                break (> 2) $ dropWhile (`elem` pickedUpIndexes) indexesToUpdate
        in unzip
               (map updatePickedUp pickedUpIndexes ++
                map updateWrap toWrap ++ map updateOthers others)
    newValues = values // updateV
    newIndexes = indexes // updateI
    updatePickedUp i =
        let v = values ! i
            newI = wrap $ i + offset - 3
        in ((newI, v), (v, newI))
    updateWrap i =
        let v = values ! i
            newI = size + i - 3
        in ((newI, v), (v, newI))
    updateOthers i =
        let v = values ! i
            newI = i - 3
        in ((newI, v), (v, newI))
-}
        

input1 = ["389125467"]

