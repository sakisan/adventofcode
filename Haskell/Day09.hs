import Control.Applicative
import Control.Monad
import Data.Ix
import Data.List
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day09.txt"
    let input = input1
    putStrLn "--- test ---" >> solve (map show ([1 .. 25] ++ [50, 51])) 25
    putStrLn "--- example ---" >> solve input1 5
    putStrLn "--- input ---" >> solve input0 25

solve :: [String] -> Int -> IO ()
solve lines preamble = do
    let numbers = map read lines :: [Int]
    let n = part1 numbers preamble
    putStr "part 1: "
    print $ n
    putStr "part 2: "
    print $ part2 numbers n

part1 :: [Int] -> Int -> Int
part1 numbers preamble =
    snd . head . filter (not . valid) $
    map (\ns -> (take preamble ns, last ns)) $
    map (\i -> take (preamble + 1) $ drop i numbers) [0 ..]

valid :: ([Int], Int) -> Bool
valid (numbers,total) =
    let l = length numbers - 1 in 
    not $ null [ 1 | (i1, i2) <- range ((0, 0), (l, l)) 
                   , i1 < i2 
                   , numbers !! i1 + numbers !! i2 == total ]

part2 :: [Int] -> Int -> Int
part2 numbers n = head list + last list
  where
    list = sort . head $ filter ((>1) . length) $  filter ((n ==) . sum) $ allSubstrings numbers

-- all substrings / contiguous sublists
allSubstrings = tail . inits <=< tails

input1 =
    [ "35"
    , "20"
    , "15"
    , "25"
    , "47"
    , "40"
    , "62"
    , "55"
    , "65"
    , "95"
    , "102"
    , "117"
    , "150"
    , "182"
    , "127"
    , "219"
    , "299"
    , "277"
    , "309"
    , "576"
    ]
