import Control.Applicative
import Control.Monad
import Data.Array
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace
import Text.Read

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day13.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve lines = do
    let numbers = parse lines
    putStr "part 1: "
    print $ part1 numbers
    putStr "part 2: "
    print $ part2 numbers

parse lines = lines

part1 lines = (depart - cap) * id
  where
    (depart, id) = head . sortOn fst $ zip (map (timeFor cap) ids) ids
    cap = traceShowId $ read $ lines !! 0 :: Int
    ids = map read . filter (/= "x") $ splitOn "," (lines !! 1) :: [Int]

timeFor cap id = head [id * x | x <- [0 ..], id * x >= cap]

part2 lines =
    traceShow ((a,b,c), start,cycle) $
    head
        [ x
        | x <- enumFromThen start (start + cycle)
        , all (check x) requirements
        ]
  where
    ids = map readMaybe $ splitOn "," (lines !! 1) :: [Maybe Int]
    requirements = sortOn snd $ filter (isJust . snd) $ zip [0 ..] ids
    -- do a first run with just the 5 biggest ids
    -- then use the result and the least common multiplier of these 5 
    -- to iterate more efficiently
    (off, (Just bigId)) = last $ sortOn snd requirements
    requirements5 =
        take 5 $ reverse $ sortOn snd $ filter (isJust . snd) $ zip [0 ..] ids
    [Just a, Just b, Just c, Just d, Just e] = map snd requirements5
    start =
        head
            [ x
            | i <- [0 ..]
            , let x = bigId * i - off
            , all (check x) requirements5
            ]
    cycle = (lcm (lcm (lcm (lcm a b) c) d) e) `div` a

check x (t, Just id) = (x + t) `mod` id == 0

input1 = ["939", "7,13,x,x,59,x,31,19"]
