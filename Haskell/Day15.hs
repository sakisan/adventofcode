{-# LANGUAGE BangPatterns #-}
import Control.Applicative
import Control.Monad
import Data.Array
import Data.Char
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace
import Text.Read
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Text.Printf

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day15.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    putStr "part 1: "
    putStr $ unlines . map (show . part1 . parse) $ input
    putStr "part 2: "
    putStr $ unlines . map (show . part2 . parse) $ input

parse :: String -> [Int]
parse = (map read . splitOn ",") 

part1 input = head $ go (length input) (reverse input)

go 2020 previous = previous
go turn (p:previous) = go (turn + 1) (x : p : previous)
  where
    x = 
        case elemIndices p (p : previous) of
            (p1:p2:_) -> p2 - p1
            _ -> 0

part2 input = go2 (length input + 1) l m
  where
    l = head (reverse input)
    m = IntMap.fromList initialized
    initialized =
        -- (zip [0 .. 30000010] (repeat 0))++
        (zip (dropLast input) [1 ..]) 
    dropLast = reverse . tail . reverse

go2 :: Int -> Int -> IntMap Int -> Int
go2 30000001 previous memory = previous
-- go2 2021 previous memory = previous
go2 turn !previous !memory = go2 (turn + 1) x (IntMap.insert previous (turn - 1) memory)
  where
    x = --(if turn `mod` 1000 == 0 then traceShow turn else id) $
        case IntMap.lookup previous memory of
            Just p2 ->  turn - p2 - 1
            _ -> 0


input1 = ["0,3,6"]

-- input1 = ["0,3,6",
--     "1,3,2",
--     "2,1,3",
--     "1,2,3",
--     "2,3,1",
--     "3,2,1",
--     "3,1,2"
--     ]
