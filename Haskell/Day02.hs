import Control.Applicative
import Data.List
import Data.List
import Data.List.Split

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day02.txt"
    let input = input0
    print $ solve1 input
    print $ solve2 input

solve1 input = length $ filter (isValid . words) input

isValid [r, [l, ':'], p] = appears >= rl && appears <= rh
  where
    appears = length (filter (== l) p)
    [rl, rh] = map read $ splitOn "-" r

solve2 input = length $ filter (isValid2 . words) input

isValid2 [r, [l, ':'], p] = 
    (p1 == l && p2 /= l) || (p1 /= l && p2 == l)
  where
    p1 = p!!(rl-1)
    p2 = p!!(rh-1)
    [rl, rh] = map read $ splitOn "-" r


input1 = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]
