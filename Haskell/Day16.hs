import Data.Ix
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day16.txt"
    -- putStrLn "--- example 1 ---" >> solve input1
    putStrLn "--- example 2 ---" >> solve input2
    putStrLn "--- input ---" >> solve input0

solve input = do
    putStr "part 1: "
    putStrLn $ show . part1 . parse $ input
    putStr "part 2: "
    putStrLn $ show . part2 . parse $ input


type Rule = (String, (Int, Int), (Int, Int))

parse :: [String] -> ([Rule], [Int], [[Int]])
parse input = (map parseRule (head sections), myTicket, otherTickets)
    where
        sections = splitOn [""] input
        myTicket = readInts ((sections !! 1) !! 1)
        otherTickets = map readInts (tail (sections !! 2))
        readInts = map read . splitOn ","

parseRule :: String -> Rule
parseRule line = (name, range1, range2)
    where
        (name, (_:_:tail1)) = break (== ':') line
        [range1, range2] = map parseRange $ splitOn " or " tail1

parseRange :: String -> (Int,Int)
parseRange range = (read a, read b)
    where [a,b] = splitOn "-" range

part1 (rules, _, tickets) = sum $ concatMap invalidInts tickets
    where
        invalidInts = mapMaybe invalid
        invalid number = if any (pass number) rules then Nothing else Just number

pass number (_, range1, range2) = inRange range1 number || inRange range2 number

part2 (rules, myTicket, tickets) = product departureFields
    where
        validTickets = filter (validTicket rules) tickets
        numberLists = transpose validTickets
        rulesPerField = (map (const rules) numberLists)
        fixedFields = shake numberLists rulesPerField []
        departureFields = 
            mapMaybe (\(i, [(ruleName, _, _)]) -> 
                if "departure" `isPrefixOf` ruleName then Just (myTicket !! i) else Nothing
            ) fixedFields

shake :: [[Int]] -> [[Rule]] -> [(Int,[Rule])] -> [(Int,[Rule])]
shake numberLists rulesPerField acc =
    -- traceShow fixedFields $
    if length fixedFields == length leftOverRules || null fixedFields then
        newAcc
    else
        shake numberLists newLeftOverRules newAcc
    where
        leftOverRules = zipWith eliminateRules numberLists rulesPerField
        fixedFields = filter ((1 ==) . length . snd) $ zip [0 ..] leftOverRules
        newLeftOverRules = map (\\ (concatMap snd fixedFields)) leftOverRules
        newAcc = acc ++ fixedFields 

validTicket rules ticket = all (\number -> any (pass number) rules) ticket

eliminateRules :: [Int] -> [Rule] -> [Rule]
eliminateRules numbers rules =
    -- traceShowId $ trace "..." $ traceShow (numbers, rules) $ 
    filter (\rule -> all (\number -> pass number rule) numbers) rules

input2 = [
    "departure class: 0-1 or 4-19",
    "departure row: 0-5 or 8-19",
    "seat: 0-13 or 16-19",
    "",
    "your ticket:",
    "11,12,13",
    "",
    "nearby tickets:",
    "3,9,18",
    "15,1,5",
    "5,14,9",
    ""
    ]

input1 = [
    "class: 1-3 or 5-7",
    "row: 6-11 or 33-44",
    "seat: 13-40 or 45-50",
    "",
    "your ticket:",
    "7,1,14",
    "",
    "nearby tickets:",
    "7,3,47",
    "40,4,50",
    "55,2,20",
    "38,6,12",
    ""
    ]



