import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Data.List
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day10.txt"
    let input = input1
    putStrLn "--- test ---" >> solve ( words "1 2 3 4 5")
    -- putStrLn "--- test ---" >> solve input1
    -- putStrLn "--- example ---" >> solve input2
    -- putStrLn "--- input ---" >> solve input0

solve lines = do
    let numbers = parse lines
    putStr "part 1: "
    print $ part1 numbers
    putStr "part 2: "
    print $ part2 numbers

parse :: [String] -> [Int]
parse lines = map read lines

part1 lines = diff1 * diff3
  where
    diff1 = length (filter (== 1) diffs)
    diff3 = length (filter (== 3) diffs) + 1
    sorted = sort (0 : lines)
    pairs = (zip <*> tail) sorted
    diffs = map (\(a, b) -> b - a) pairs

part2 lines = fst $ count target IntMap.empty (sorted)
  where
    target = last sorted + 3
    sorted = sort (0 : lines)

count :: Int -> IntMap Int -> [Int] -> (Int, IntMap Int)
count target seen [] = (0, seen)
count target seen [x]
    | target - x <= 3 = (1, seen)
    | otherwise = (0, seen)
count target seen (x:xs) =
    case IntMap.lookup x seen of
        Just a -> (a, seen)
        Nothing -> (thisCount, IntMap.insert x thisCount thisSeen)
            where validNext = takeWhile (\y -> y - x <= 3) xs
                  nextList y = dropWhile (<= y) xs
                  (thisCount, thisSeen) =
                      foldr
                          (\y (acc, accSeen) ->
                               let (n, updatedSeen) =
                                       count target accSeen (y : nextList y)
                                   newSeen = IntMap.insert y n updatedSeen
                               in (n + acc, newSeen))
                          (0, seen)
                          validNext


input1 = ["16", "10", "15", "5", "1", "11", "7", "19", "6", "12", "4"]
input2 = [ "28" , "33" , "18" , "42" , "31" , "14" , "46" , "20" , "48" , "47" , "24" , "23" , "49" , "45" , "19" , "38" , "39" , "11" , "1" , "32" , "25" , "35" , "8" , "17" , "7" , "9" , "4" , "2" , "34" , "10" , "3" ]
