import Data.List
import Debug.Trace


main :: IO ()
main = do
    input0 <- lines <$> readFile "Day25.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1: "
    print $ part1 parsed

type Input = (Int,Int)

parse :: [String] -> Input
parse [a, b] = (read a, read b)

part1 :: Input -> Int
part1 (card, door) = 
    traceShow (loopSizeCard,loopSizeDoor) $
    (iterate (transform door) 1) !! loopSizeCard
    where 
        Just loopSizeCard = elemIndex card transformations
        Just loopSizeDoor = elemIndex door transformations
        transformations = iterate (transform 7) 1

    
transform :: Int -> Int -> Int
transform subject value = 
    (subject * value ) `mod` 20201227

input1 = [
    "5764801",
    "17807724"
    ]
