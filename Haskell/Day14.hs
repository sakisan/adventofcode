import Control.Applicative
import Control.Monad
import Data.Array
import Data.Char
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace
import Text.Read
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Text.Printf

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day14.txt"
    -- putStrLn "--- example ---" >> solve input1
    putStrLn "--- example ---" >> solve input2
    putStrLn "--- input ---" >> solve input0

solve lines = do
    putStr "part 1: "
    print $ part1 lines
    putStr "part 2: "
    print $ part2 lines

type Memory = IntMap Int
type Mask = String

part1 (line:lines) = sum $ IntMap.elems memory
    where (memory, mask) = foldl' (processLine) (IntMap.empty, toMask line) lines

processLine :: (Memory, Mask) -> String -> (Memory, Mask)
processLine (memory, mask) line 
    | "mask = " `isPrefixOf` line = (memory, toMask line)
    | otherwise = (apply address value mask memory, mask)
    where 
        address = read $ takeWhile isDigit $ drop 4 line :: Int
        value = read $ drop 2 $ dropWhile (/= '=') line :: Int

apply :: Int -> Int -> Mask -> Memory -> Memory
apply address value mask memory = 
    IntMap.insert address (applyMask mask value) memory

applyMask :: Mask -> Int -> Int
applyMask mask value =
    fromBinary $ zipWith applyBit mask paddedBinary
    where 
        paddedBinary =  (take (length mask - length binary) $ repeat '0') ++ binary
        binary =  toBinary value

applyBit :: Char -> Char -> Char
applyBit 'X' b = b
applyBit x _ = x

toMask :: String -> Mask
toMask line = drop (length "mask = ") line

-- binary to decimal
fromBinary=foldl(\a x->2*a+read[x])0
-- decimal to binary
toBinary n= printf "%b" n


-- PART 2

part2 (line:lines) = sum $ IntMap.elems memory
    where (memory, mask) = foldl' (processLine2) (IntMap.empty, toMask line) lines

processLine2 :: (Memory, Mask) -> String -> (Memory, Mask)
processLine2 (memory, mask) line 
    | "mask = " `isPrefixOf` line = (memory, toMask line)
    | otherwise = (apply2 address value mask memory, mask)
    where 
        address = read $ takeWhile isDigit $ drop 4 line :: Int
        value = read $ drop 2 $ dropWhile (/= '=') line :: Int

apply2 :: Int -> Int -> Mask -> Memory -> Memory
apply2 address value mask memory = 
    foldr (\a -> IntMap.insert a value) memory (addresses mask address)

addresses :: Mask -> Int -> [Int]
addresses mask address = map (\p -> applyMask2 p mask address) ps
    where
        numberOfXs = length $ filter (== 'X') mask
        ps = combinationsWithRep numberOfXs "01"
        
applyMask2 :: [Char] -> Mask -> Int -> Int
applyMask2 replacements mask value =
    fromBinary $ applyReplacements replacements $ zipWith applyBit2 mask paddedBinary
    where 
        paddedBinary = (take (length mask - length binary) $ repeat '0') ++ binary
        binary = toBinary value

applyBit2 :: Char -> Char -> Char
applyBit2 'X' _ = 'X'
applyBit2 '1' _ = '1'
applyBit2 '0' b = b

applyReplacements :: String -> String -> String
applyReplacements [] v = v
applyReplacements _ [] = []
applyReplacements (r:replacements) ('X':xs) = r : (applyReplacements replacements xs)
applyReplacements replacements (x:xs) = x : (applyReplacements replacements xs)
    

-- all the ways to take n elements out of a list (with repetitions)
combinationsWithRep :: Int -> [a] -> [[a]]
combinationsWithRep n list = mapM (\_ -> list) [1..n]
    

input1 = [
    "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
    "mem[8] = 11",
    "mem[7] = 101",
    "mem[8] = 0"
    ]


input2 = [
    "mask = 000000000000000000000000000000X1001X",
    "mem[42] = 100",
    "mask = 00000000000000000000000000000000X0XX",
    "mem[26] = 1"
    ]
