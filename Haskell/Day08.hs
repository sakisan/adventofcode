import Data.Either
import Data.List
import Data.Maybe
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day08.txt"
    let input = input0
    let instructions = zip [0 ..] input
    print $ solve1 instructions
    print $ solve2 instructions

solve1 :: [(Int, String)] -> Either Int Int
solve1 = go [] (0, 0)

solve2 :: [(Int, String)] -> Int
solve2 instructions = head . rights $ map (try instructions) [0 ..]

try :: [(Int, String)] -> Int -> Either Int Int
try instructions index = maybe (Left 0) solve1 $ toggle =<< lookup index instructions
  where
    toggle = toggle' . take 3
    toggle' "nop" = Just $ update "jmp"
    toggle' "jmp" = Just $ update "nop"
    toggle' _ = Nothing
    update replacement = as ++ [(index, replacement ++ drop 3 instruction)] ++ bs
    (as, (_, instruction):bs) = splitAt index instructions

go :: [Int] -> (Int, Int) -> [(Int, String)] -> Either Int Int
go prev (value, pointer) instructions
    | pointer `elem` prev = Left value
    | otherwise = maybe (Right value) goNext $ lookup pointer instructions
  where
    goNext line =
        go (pointer : prev) (doLine value pointer (words line)) instructions

doLine value pointer ["nop", _] = (value, pointer + 1)
doLine value pointer ["jmp", i] = (value, pointer + read' i)
doLine value pointer ["acc", i] = (value + read' i, pointer + 1)

read' ('+':i) = read i :: Int
read' ('-':i) = negate $ read i :: Int

input1 =
    [ "nop +0"
    , "acc +1"
    , "jmp +4"
    , "acc +3"
    , "jmp -3"
    , "acc -99"
    , "acc +1"
    , "jmp -4"
    , "acc +6"
    ]
