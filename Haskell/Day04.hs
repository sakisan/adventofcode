import Control.Applicative
import Data.Array
import Data.Char
import Data.List
import Data.List.Split
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day04.txt"
    -- input0 <- lines <$> readFile "valid.txt"
    -- input0 <- lines <$> readFile "invalid.txt"
    let input = input0
    print $ solve1 input
    print $ solve2 input

fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"]

solve1 input = length $ filter isValid passports
  where
    passports = filter (not . null) $ splitOn [[]] input

isValid passport = sort (withoutCid fs) == sort (withoutCid fields)
  where
    fs =
        map (head . splitOn ":") $
        filter (':' `elem`) $ words $ unlines passport

withoutCid = filter (/= "cid")

solve2 input = length $ filter isValid2 passports
  where
    passports = filter (not . null) $ splitOn [[]] input

isValid2 passport =
    -- traceShow (zip (map validField part2) part2) $
    isValid passport && all validField part2
  where
    part2 = map (splitOn ":") $ filter (':' `elem`) $ words $ unlines passport

validField ["cid", value] = True
validField ["byr", value] = length value == 4 && all isDigit value && between 1920 2020 (read value)
validField ["iyr", value] = length value == 4 && all isDigit value && between 2010 2020 (read value)
validField ["eyr", value] = length value == 4 && all isDigit value && between 2020 2030 (read value)
validField ["hgt", value] = cm (reverse value) || inches (reverse value)
  where
    cm ('m':'c':v) = length v == 3 && all isDigit v && between 150 193 (read $ reverse v)
    cm _ = False
    inches ('n':'i':v) = length v == 2 && all isDigit v && between 59 76 (read $ reverse v)
    inches _ = False
validField ["hcl", ('#':value)] = length value == 6 && all isHexDigit value
validField ["ecl", value] = value `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
validField ["pid", value] = length value == 9 && all isDigit value
validField _ = False

between low high v = low <= v && v <= high

input1 =
    [ "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd"
    , "byr:1937 iyr:2017 cid:147 hgt:183cm"
    , ""
    , "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884"
    , "hcl:#cfa07d byr:1929"
    , ""
    , "hcl:#ae17e1 iyr:2013"
    , "eyr:2024"
    , "ecl:brn pid:760753108 byr:1931"
    , "hgt:179cm"
    , ""
    , "hcl:#cfa07d eyr:2025 pid:166559648"
    , "iyr:2011 ecl:brn hgt:59in"
    , ""
    ]
