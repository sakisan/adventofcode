import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day07.txt"
    let input = input0
    print $ solve1 input
    print $ solve2 input

solve1 lines =
    length $ {- traceShowId $ -} filter (findGold totalMap) $ Map.keys totalMap
  where
    totalMap =
        -- traceShowId $
        foldr (Map.unionWith (++)) Map.empty $ map (toMap . parseLine) lines
    toMap (bag, outerBags) =
        foldr
            (\(n, outer) acc -> Map.insertWith (++) bag [outer] acc)
            Map.empty
            outerBags

findGold m k =
    case Map.lookup k m of
        Nothing -> False
        Just list ->
            if "shiny gold" `elem` list
                then True
                else any (findGold m) list

parseLine line = (bag, otherBags)
  where
    w = words line
    bag = unwords $ take 2 w
    otherBags = contains (drop 4 w)
    contains ("no":bags) = []
    contains (n:b1:b2:more) =
        (read n :: Int, b1 ++ " " ++ b2) : contains (drop 1 more)
    contains [] = []

solve2 lines = sum . map fst $ {- traceShowId $ -} findAll totalMap "shiny gold"
  where
    totalMap =
        foldr (Map.unionWith (++)) Map.empty $ map (toMap . parseLine) lines
    toMap (bag, outerBags) =
        foldr
            (\outer acc -> Map.insertWith (++) bag [outer] acc)
            Map.empty
            outerBags

findAll m k =
    case Map.lookup k m of
        Nothing -> []
        Just bags ->
            bags ++ concat (map (\(n, b) -> map (times n) $ findAll m b) bags)

times n (a, b) = (n * a, b)

input1 =
    [ "light red bags contain 1 bright white bag, 2 muted yellow bags."
    , "dark orange bags contain 3 bright white bags, 4 muted yellow bags."
    , "bright white bags contain 1 shiny gold bag."
    , "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags."
    , "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags."
    , "dark olive bags contain 3 faded blue bags, 4 dotted black bags."
    , "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags."
    , "faded blue bags contain no other bags."
    , "dotted black bags contain no other bags."
    , ""
    ]

input2 =
    [ "shiny gold bags contain 2 dark red bags."
    , "dark red bags contain 2 dark orange bags."
    , "dark orange bags contain 2 dark yellow bags."
    , "dark yellow bags contain 2 dark green bags."
    , "dark green bags contain 2 dark blue bags."
    , "dark blue bags contain 2 dark violet bags."
    , "dark violet bags contain no other bags."
    , ""
    ]
