import Data.Array
import Data.List
import Data.List.Split
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day20.txt"
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1: "
    print $ part1 parsed
    putStr "part 2: "
    print $ part2 parsed

type Input = [Tile]

type Tile = (Int, Image)

type Image = Array (Int, Int) Char

parse :: [String] -> [Tile]
parse = map parseTile . filter (not . null) . splitOn [[]]

parseTile :: [String] -> Tile
--          T  i  l  e     2   3   1   1   :
parseTile ([_, _, _, _, _, d1, d2, d3, d4, _]:image) =
    (read [d1, d2, d3, d4] :: Int, parseArray image)

-- part 1
part1 :: Input -> Int
part1 = product . traceShowId . cornerIds

cornerIds :: Input -> [Int]
cornerIds input = map fst . take 4 . sortOn countCommon $ allEdges
  where
    displayTiles =
        unlines $ map (\(id, image) -> unlines [show id, showArray image]) input
    displayEdges = unlines $ map (unlines . snd) allEdges
    (id1, tile1) = head input
    allEdges = map (fmap edges) input
    countCommon (id, es) =
        sum . map (countCommon1 es) . map snd $ filter ((/=) id . fst) allEdges

countCommon1 :: [String] -> [String] -> Int
countCommon1 as bs = max straight flipped
  where
    straight = length $ filter (`elem` as) (map reverse bs)
    flipped = length $ filter (`elem` as) bs

edges :: Image -> [String]
edges image =
    map topEdge $ [id, rotateCCW, rotateCCW . rotateCCW, rotateCW] <*> [image]
  where
    (_, (h, w)) = bounds image
    topEdge = take w . elems

-- part 2
part2 :: Input -> Int
part2 input = trace displayTiles $ traceShow ("monsters found", found) $ countXs
  where
    displayTiles = showArray bigImage
    cornerId = head $ cornerIds input
    ([cornerTile], tiles) = partition ((== cornerId) . fst) input
    reference = ((0, 0), cornerTile)
    allOrientated =
        orientateAll [reference] (deleteById cornerTile tiles) [reference]
    deleteById = deleteBy (\(id1, _) (id2, _) -> id1 == id2)
    allMerged =
        mergeAll $
        map (uncurry renumber . fmap (removeBorder . snd)) allOrientated
    (found, bigImage) =
        head . filter ((> 0) . fst) . map markMonsters $
        [id, rotateCCW, rotateCCW . rotateCCW, rotateCW] <*>
        [allMerged, flipHorizontally allMerged]
    countXs = length $ filter ('#' ==) $ elems bigImage

monster :: [String]
monster =
    [ "                  # " --
    , "#    ##    ##    ###" -- 
    , " #  #  #  #  #  #   " --
    ]

monsterIndices :: [(Int, Int)]
monsterIndices =
    map (zeroBased . fst) . filter (('#' ==) . snd) . assocs . parseArray $
    monster
  where
    zeroBased (y, x) = (y - 1, x - 1)

markMonsters :: Image -> (Int, Image)
markMonsters image = (length monsters, foldr markMonster image monsters)
  where
    monsters = filter hasMonster (indices image)
    hasMonster = all (`elem` "#O") . map safeGet . appliedMonsterIndices
    appliedMonsterIndices i = map (add i) monsterIndices
    safeGet i
        | inRange (bounds image) i = image ! i
        | otherwise = 'X'
    add (y, x) (dy, dx) = (y + dy, x + dx)
    markMonster i acc = acc // [(j, 'O') | j <- appliedMonsterIndices i]

orientateAll ::
       [((Int, Int), Tile)]
    -> [Tile]
    -> [((Int, Int), Tile)]
    -> [((Int, Int), Tile)]
orientateAll _ [] acc = acc
orientateAll (reference:queue) tiles acc =
    orientateAll
        (queue ++ toAdd)
        (notAdjacents ++ map snd dontAdd)
        (acc ++ toAdd)
  where
    (adjacents, notAdjacents) =
        partition (adjacentTiles (snd $ reference)) tiles
    orientated = map (orientateTo reference) adjacents
    (toAdd, dontAdd) = partition (matchOthers acc) orientated

matchOthers :: [((Int, Int), Tile)] -> ((Int, Int), Tile) -> Bool
matchOthers others ((y, x), (_, image)) = all matches others
  where
    matches ((otherY, otherX), (_, otherImage))
        | (y == otherY && x == otherX - 1) = check 1 3
        | (y == otherY && x == otherX + 1) = check 3 1
        | (y == otherY - 1 && x == otherX) = check 2 0
        | (y == otherY + 1 && x == otherX) = check 0 2
        | otherwise = True
      where
        check e1 e2 = tileEdges !! e1 == reverse (otherEdges !! e2)
        tileEdges = edges image
        otherEdges = edges otherImage

orientateTo :: ((Int, Int), Tile) -> Tile -> ((Int, Int), Tile)
orientateTo ((yRef, xRef), (refId, reference)) (tileId, adjacent) =
    -- traceShow (refId, tileId, aIndex, bIndex, (yRef, xRef), (yT, xT)) $
    -- traceShow (yT, xT) $
    -- trace ("reference\n" ++ showArray reference) $
    -- trace ("adjacent\n" ++ showArray adjacent) $
    ((yT, xT), (tileId, transformation adjacent))
  where
    as = (edges reference)
    bs = (edges adjacent)
    straight = filter (`elem` as) (map reverse bs)
    flipped = filter (`elem` as) bs
    [commonEdge] = straight ++ flipped
    Just aIndex = elemIndex commonEdge as
    Just bIndex = elemIndex commonEdge ((map reverse bs) ++ bs)
    transformationIndex = (4 * 4 + aIndex - bIndex) `mod` 4
    transformations :: [Image -> Image]
    transformations =
        map
            ((rotateCCW . rotateCCW) .)
            [id, rotateCW, rotateCCW . rotateCCW, rotateCCW] -- transposed transformations
    transformation :: Image -> Image
    transformation = f (transformations !! transformationIndex)
      where
        f
            | bIndex > 4 && even bIndex = (flipHorizontally .)
            | bIndex > 4 = (flipVertically .)
            | otherwise = (id .)
    (dy, dx) = [(-1, 0), (0, 1), (1, 0), (0, -1)] !! aIndex
    (yT, xT) = (yRef + dy, xRef + dx)

adjacentTiles :: Tile -> Tile -> Bool
adjacentTiles (_, a) (_, b) = countCommon1 (edges a) (edges b) == 1

removeBorder :: Image -> Image
removeBorder image = array ((2, 2), (9, 9)) filtered
  where
    filtered = filter isNotBorder $ assocs image
    isNotBorder ((y, x), c) = x `notElem` [1, 10] && y `notElem` [1, 10]

renumber :: (Int, Int) -> Image -> Image
renumber (yT, xT) image = array newBounds (map reposition (assocs image))
  where
    ((y0, x0), (y1, x1)) = bounds image
    (h, w) = (y1 - y0 + 1, x1 - x0 + 1)
    (newY0, newX0) = (yT * h, xT * w)
    newBounds = ((newY0, newX0), (newY0 + h - 1, newX0 + w - 1))
    reposition ((y, x), v) = ((newY0 + y - y0, newX0 + x - x0), v)

mergeAll :: [Image] -> Image
mergeAll images = array ((yMin, xMin), (yMax, xMax)) allAssocs
  where
    allAssocs = concatMap assocs images
    (indices, values) = unzip allAssocs
    (ys, xs) = unzip indices
    (yMax, xMax) = (maximum ys, maximum xs)
    (yMin, xMin) = (minimum ys, minimum xs)

-- plumbing
rotateCCW :: Image -> Image
rotateCCW image = array ((x0, y0), (x1, y1)) rearranged
  where
    ((y0, x0), (y1, x1)) = bounds image
    rearranged = map rearrange $ assocs image
    rearrange ((y, x), c) = ((x1 - x + x0, y), c)

rotateCW :: Image -> Image
rotateCW = rotateCCW . rotateCCW . rotateCCW

flipHorizontally :: Image -> Image
flipHorizontally image = array (bounds image) rearranged
  where
    ((y0, x0), (y1, x1)) = bounds image
    rearranged = map rearrange $ assocs image
    rearrange ((y, x), c) = ((y, x1 - x + x0), c)

flipVertically :: Image -> Image
flipVertically image = array (bounds image) rearranged
  where
    ((y0, x0), (y1, x1)) = bounds image
    rearranged = map rearrange $ assocs image
    rearrange ((y, x), c) = ((y1 - y + y0, x), c)

parseArray :: [String] -> Array (Int, Int) Char
parseArray input = listArray ((1, 1), (h, w)) (concat input)
  where
    h = length input
    w = length (input !! 0)

showArray :: Array (Int, Int) Char -> String
showArray a = unlines $ [] : chunksOf w (elems a) ++ [show (bounds a)]
  where
    ((y0, x0), (y1, x1)) = bounds a
    (h, w) = (y1 - y0 + 1, x1 - x0 + 1)

input1 =
    [ "Tile 2311:"
    , "..##.#..#."
    , "##..#....."
    , "#...##..#."
    , "####.#...#"
    , "##.##.###."
    , "##...#.###"
    , ".#.#.#..##"
    , "..#....#.."
    , "###...#.#."
    , "..###..###"
    , ""
    , "Tile 1951:"
    , "#.##...##."
    , "#.####...#"
    , ".....#..##"
    , "#...######"
    , ".##.#....#"
    , ".###.#####"
    , "###.##.##."
    , ".###....#."
    , "..#.#..#.#"
    , "#...##.#.."
    , ""
    , "Tile 1171:"
    , "####...##."
    , "#..##.#..#"
    , "##.#..#.#."
    , ".###.####."
    , "..###.####"
    , ".##....##."
    , ".#...####."
    , "#.##.####."
    , "####..#..."
    , ".....##..."
    , ""
    , "Tile 1427:"
    , "###.##.#.."
    , ".#..#.##.."
    , ".#.##.#..#"
    , "#.#.#.##.#"
    , "....#...##"
    , "...##..##."
    , "...#.#####"
    , ".#.####.#."
    , "..#..###.#"
    , "..##.#..#."
    , ""
    , "Tile 1489:"
    , "##.#.#...."
    , "..##...#.."
    , ".##..##..."
    , "..#...#..."
    , "#####...#."
    , "#..#.#.#.#"
    , "...#.#.#.."
    , "##.#...##."
    , "..##.##.##"
    , "###.##.#.."
    , ""
    , "Tile 2473:"
    , "#....####."
    , "#..#.##..."
    , "#.##..#..."
    , "######.#.#"
    , ".#...#.#.#"
    , ".#########"
    , ".###.#..#."
    , "########.#"
    , "##...##.#."
    , "..###.#.#."
    , ""
    , "Tile 2971:"
    , "..#.#....#"
    , "#...###..."
    , "#.#.###..."
    , "##.##..#.."
    , ".#####..##"
    , ".#..####.#"
    , "#..#.#..#."
    , "..####.###"
    , "..#.#.###."
    , "...#.#.#.#"
    , ""
    , "Tile 2729:"
    , "...#.#.#.#"
    , "####.#...."
    , "..#.#....."
    , "....#..#.#"
    , ".##..##.#."
    , ".#.####..."
    , "####.#.#.."
    , "##.####..."
    , "##..#.##.."
    , "#.##...##."
    , ""
    , "Tile 3079:"
    , "#.#.#####."
    , ".#..######"
    , "..#......."
    , "######...."
    , "####.#..#."
    , ".#...#.##."
    , "#.#####.##"
    , "..#.###..."
    , "..#......."
    , "..#.###..."
    , ""
    ]
