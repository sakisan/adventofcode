import Data.List
import Control.Applicative

main :: IO ()
main = do
    input0 <- readFile "Day01.txt"
    let input = input0
    let numbers = map read $ lines input
    putStrLn $ show $ solve1 numbers
    putStrLn $ show $ solve2 numbers

solve1 numbers = x * y
    where (x,y) = head $
                filter (\(a,b) -> a + b == 2020) $
                liftA2 (,) numbers numbers 


solve2 numbers = x * y * z
    where (x,y,z) = head $
                filter (\(a,b,c) -> a + b + c == 2020) $
                liftA3 (,,) numbers numbers numbers

input1 = "1721\n\
\979\n\
\366\n\
\299\n\
\675\n\
\1456"
