import Control.Applicative
import Control.Monad
import Data.Array
import Data.List
import Data.List.Split
import Data.Maybe
import Debug.Trace

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day12.txt"
    let input = input1
    putStrLn "--- example ---" >> solve input1
    putStrLn "--- input ---" >> solve input0

solve lines = do
    let numbers = parse lines
    putStr "part 1: "
    print $ part1 numbers
    putStr "part 2: "
    print $ part2 numbers
    

parse lines = lines

part1 lines = manhattan (0,0) p
    where 
        (p,d) = foldl f ((0,0),east) lines
        f (p,d) line = moveLine p d line


moveLine position direction (x:xs) =
    case x of
        'N' -> (add position $ times a north, direction)
        'S' -> (add position $ times a south, direction)
        'E' -> (add position $ times a east,  direction)
        'W' -> (add position $ times a west,  direction)
        'L' -> (position,  (left a direction))
        'R' -> (position, (right a direction))
        'F' -> (add position $ times a direction, direction)
    where a = read xs :: Int


part2 lines' = manhattan (0,0) p
    where 
        lines = lines'
        (p,d) = foldl f ((0,0),(-1,-10)) lines
        f (p,d) line = moveLine2 p d line

moveLine2 position waypoint (x:xs) =
    --traceShow ("   ",position, waypoint, relative) $ traceShow (x:xs) $
    case x of
        'N' -> (position, add waypoint $ times a north)
        'S' -> (position, add waypoint $ times a south)
        'E' -> (position, add waypoint $ times a  east)
        'W' -> (position, add waypoint $ times a  west)
        'L' -> (position, rotateLeft  a position waypoint)
        'R' -> (position, rotateRight a position waypoint)
        'F' -> (forwarded, add forwarded relative)
    where 
        a = read xs :: Int
        relative = unadd position waypoint
        forwarded = add position $ times a relative

rotateLeft a position = add position . left a . unadd position 
rotateRight a position = add position . right a . unadd position 

north = (-1,0)
south = (1,0)
east = (0,-1)
west = (0,1)

turnLeft (y,x) = (x, -y)
turnRight (y,x) = (-x, y) 

left 90 d = turnLeft d
left n d = turnLeft $ left (n - 90) d

right 90 = turnRight
right n = turnRight . right (n - 90)

add :: (Int,Int) -> (Int,Int) -> (Int,Int)
add (a,b) (i,j) = (a + i, b + j)    

unadd :: (Int,Int) -> (Int,Int) -> (Int,Int)
unadd (a,b) (i,j) = (i - a, j - b)    

times :: Int -> (Int,Int) -> (Int,Int)
times i (a,b) = (a * i, b * i)    


-- manhattan distance
manhattan :: (Int,Int) -> (Int,Int) -> Int
manhattan (x,y) (x',y') = abs (x - x') + abs (y - y')

input1 = [

    "F10",
    "N3",
    "F7",
    "R90",
    "F11"
    ]
