{-# LANGUAGE BangPatterns #-}

import Control.Applicative
import Control.Monad
import Data.Array
import Data.Char
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Sequence (Seq(..), (><), (|>))
import qualified Data.Sequence as Seq
import Data.Set (Set)
import qualified Data.Set as Set
import Debug.Trace
import Text.Printf
import Text.Read

main :: IO ()
main = do
    input0 <- lines <$> readFile "Day22.txt"
    putStrLn "--- example ---" >> solve input1
    -- putStrLn "--- example ---" >> solve input2
    putStrLn "--- input ---" >> solve input0

solve input = do
    let parsed = parse input
    putStr "part 1: "
    print $ part1 parsed
    putStr "part 2: "
    print $ part2 parsed

type Input = ([Int], [Int])

parse :: [String] -> Input
parse input = (deck1, deck2)
  where
    [deck1, deck2] = map (parseDeck . tail) . splitOn [[]] $ input
    parseDeck cards = map read cards :: [Int]

-- part1 :: Input -> Int
part1 = score . playOut

score = sum . zipWith (*) [1 ..] . reverse

part2 = score . fromEithers . playRecursive Set.empty

playOut ([], b) = b
playOut (a, []) = a
playOut ((a:as), (b:bs)) =
    if a > b
        then playOut (as ++ [a, b], bs)
        else playOut (as, bs ++ [b, a])

playRecursive seen ([], bs) = Right bs
playRecursive seen (as, []) = Left as
playRecursive seen configuration@((a:as), (b:bs)) =
    if Set.member configuration seen
        then Left (a : as)
        else if length as >= a && length bs >= b
                 then if aWinsSubgame
                          then aWinsRound
                          else bWinsRound
                 else if a > b
                          then aWinsRound
                          else bWinsRound
  where
    aWinsRound = playRecursive seen' (as ++ [a, b], bs)
    bWinsRound = playRecursive seen' (as, bs ++ [b, a])
    seen' = Set.insert configuration seen
    aWinsSubgame = 
        case playRecursive Set.empty (take a as, take b bs) of
            Left _ -> True
            Right _ -> False

fromEithers :: Either a a -> a
fromEithers (Left a) = a
fromEithers (Right a) = a

input1 =
    [ "Player 1:"
    , "9"
    , "2"
    , "6"
    , "3"
    , "1"
    , ""
    , "Player 2:"
    , "5"
    , "8"
    , "4"
    , "7"
    , "10"
    ]

input2 = ["Player 1:", "43", "19", "", "Player 2:", "2", "29", "14"]


